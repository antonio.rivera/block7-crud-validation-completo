package com.example.block7crudvalidation.profesor.infrastructure.controller.dto.input;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProfesorInputDtoTest {
    @Test
    void getSetId_profesorInputDto() {
        ProfesorInputDto profesorInputDto = new ProfesorInputDto();
        profesorInputDto.setId_profesor(1);
        assertEquals(1,profesorInputDto.getId_profesor());
    }

    @Test
    void getSetComents() {
        ProfesorInputDto profesorInputDto = new ProfesorInputDto();
        profesorInputDto.setComents("bueno");
        assertEquals("bueno",profesorInputDto.getComents());
    }

    @Test
    void getSetBranch() {
        ProfesorInputDto profesorInputDto = new ProfesorInputDto();
        profesorInputDto.setBranch("ciencia");
        assertEquals("ciencia",profesorInputDto.getBranch());
    }

    @Test
    void getSetId_persona() {
        ProfesorInputDto profesorInputDto = new ProfesorInputDto();
        profesorInputDto.setId_persona(1);
        assertEquals(1,profesorInputDto.getId_persona());
    }

}