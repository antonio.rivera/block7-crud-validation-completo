package com.example.block7crudvalidation.profesor.infrastructure.controller.dto.output;

import com.example.block7crudvalidation.persona.domain.entity.Persona;
import com.example.block7crudvalidation.persona.domain.enums.Role;
import org.junit.jupiter.api.Test;

import java.util.Calendar;

import static org.junit.jupiter.api.Assertions.*;

class ProfesorOutputDtoTest {
    @Test
    void getSetId_profesorOutputDto() {
        ProfesorOutputDto profesorOutputDto = new ProfesorOutputDto();
        profesorOutputDto.setId_profesor(1);
        assertEquals(1,profesorOutputDto.getId_profesor());
    }

    @Test
    void getSetComents() {
        ProfesorOutputDto profesorOutputDto = new ProfesorOutputDto();
        profesorOutputDto.setComents("bueno");
        assertEquals("bueno",profesorOutputDto.getComents());
    }

    @Test
    void getSetBranch() {
        ProfesorOutputDto profesorOutputDto = new ProfesorOutputDto();
        profesorOutputDto.setBranch("ciencia");
        assertEquals("ciencia",profesorOutputDto.getBranch());
    }

    @Test
    void getSetId_persona() {
        ProfesorOutputDto profesorOutputDto = new ProfesorOutputDto();
        profesorOutputDto.setId_persona(1);
        assertEquals(1,profesorOutputDto.getId_persona());
    }

    @Test
    void getPersona() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024,5,20);
        Calendar c2 = Calendar.getInstance();
        c2.set(2050,5,20);
        Persona persona = new Persona(1,"juan123","1234","juan","lopez","juancompany@gmail.com","juanlopez@gmail.com","jodar",true, c1.getTime(),"http//:imagen.com",c2.getTime(), Role.USER,2,2);
        ProfesorOutputDto profesorOutputDto = new ProfesorOutputDto();
        profesorOutputDto.setPersona(persona);
        assertEquals(profesorOutputDto.getPersona(),persona);
    }

}