package com.example.block7crudvalidation.profesor.application.mappers;

import com.example.block7crudvalidation.asignatura.domain.entity.Asignatura;
import com.example.block7crudvalidation.persona.domain.entity.Persona;
import com.example.block7crudvalidation.persona.domain.enums.Role;
import com.example.block7crudvalidation.profesor.domain.entity.Profesor;
import com.example.block7crudvalidation.profesor.infrastructure.controller.dto.input.ProfesorInputDto;
import com.example.block7crudvalidation.profesor.infrastructure.controller.dto.output.ProfesorOutputDto;
import com.example.block7crudvalidation.student.domain.entity.Student;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
@SpringBootTest
class ProfesorDtoConverterTest {
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private ProfesorDtoConverter profesorDtoConverter;


    @Test
    void convertInputToProfesor() {
        // Crear ProfesorInputDto
        Calendar c1 = Calendar.getInstance();
        c1.set(2024,5,20);
        Calendar c2 = Calendar.getInstance();
        c2.set(2050,5,20);
        Persona persona = new Persona(1,"juan123","1234","juan","lopez","juancompany@gmail.com","juanlopez@gmail.com","jodar",true, c1.getTime(),"http//:imagen.com",c2.getTime(), Role.USER,2,2);
        Asignatura asignatura = new Asignatura(1,"lengua","castellano", LocalDate.of(2024,5,10),LocalDate.of(2025,5,10));
        List<Asignatura> asignaturaList = new ArrayList<>();
        asignaturaList.add(asignatura);
        Student student = new Student(1,10,"bueno","ciencia",persona,asignaturaList,new Profesor());
        List<Student> studentList = new ArrayList<>();
        studentList.add(student);
        ProfesorInputDto profesorInputDto = new ProfesorInputDto(1,"bueno","back",1);
        // Crear Profesor
        Profesor profesor = new Profesor(1,"bueno","back",persona,studentList);
        // Ejecucion del modelMapper
        Profesor profesor1 = modelMapper.map(profesorInputDto,Profesor.class);
        // Ejecucion de la funcion convertInputToProfesor
        Profesor profesor2 = profesorDtoConverter.convertInputToProfesor(profesorInputDto);
        // Comparar
        assertEquals(profesor1.toString(),profesor2.toString());
    }

    @Test
    void converToSimpleOutput() {
        // Crear ProfesorInputDto
        Calendar c1 = Calendar.getInstance();
        c1.set(2024,5,20);
        Calendar c2 = Calendar.getInstance();
        c2.set(2050,5,20);
        Persona persona = new Persona(1,"juan123","1234","juan","lopez","juancompany@gmail.com","juanlopez@gmail.com","jodar",true, c1.getTime(),"http//:imagen.com",c2.getTime(), Role.USER,2,2);
        Asignatura asignatura = new Asignatura(1,"lengua","castellano", LocalDate.of(2024,5,10),LocalDate.of(2025,5,10));
        List<Asignatura> asignaturaList = new ArrayList<>();
        asignaturaList.add(asignatura);
        Student student = new Student(1,10,"bueno","ciencia",persona,asignaturaList,new Profesor());
        List<Student> studentList = new ArrayList<>();
        studentList.add(student);
        ProfesorInputDto profesorInputDto = new ProfesorInputDto(1,"bueno","back",1);
        // Crear Profesor
        Profesor profesor = new Profesor(1,"bueno","back",persona,studentList);
        // Ejecucion del modelMapper
        ProfesorInputDto profesor1 = modelMapper.map(profesor,ProfesorInputDto.class);
        // Ejecucion de la funcion converToSimpleOutput
        ProfesorInputDto profesor2 = profesorDtoConverter.converToSimpleOutput(profesor);
        // Comparar
        assertEquals(profesor1.toString(),profesor2.toString());
    }

    @Test
    void converToFullOutput() {
        // Crear ProfesorInputDto
        Calendar c1 = Calendar.getInstance();
        c1.set(2024,5,20);
        Calendar c2 = Calendar.getInstance();
        c2.set(2050,5,20);
        Persona persona = new Persona(1,"juan123","1234","juan","lopez","juancompany@gmail.com","juanlopez@gmail.com","jodar",true, c1.getTime(),"http//:imagen.com",c2.getTime(), Role.USER,2,2);
        Asignatura asignatura = new Asignatura(1,"lengua","castellano", LocalDate.of(2024,5,10),LocalDate.of(2025,5,10));
        List<Asignatura> asignaturaList = new ArrayList<>();
        asignaturaList.add(asignatura);
        Student student = new Student(1,10,"bueno","ciencia",persona,asignaturaList,new Profesor());
        List<Student> studentList = new ArrayList<>();
        studentList.add(student);
        ProfesorOutputDto profesorOutputDto = new ProfesorOutputDto(1,"bueno","back",1,persona);
        // Crear Profesor
        Profesor profesor = new Profesor(1,"bueno","back",persona,studentList);
        // Ejecucion del modelMapper
        ProfesorOutputDto profesor1 = modelMapper.map(profesor,ProfesorOutputDto.class);
        // Ejecucion de la funcion converToSimpleOutput
        ProfesorOutputDto profesor2 = profesorDtoConverter.converToFullOutput(profesor);
        // Comparar
        assertEquals(profesor1.toString(),profesor2.toString());
    }
}