package com.example.block7crudvalidation.profesor.application.impl;


import com.example.block7crudvalidation.asignatura.domain.entity.Asignatura;
import com.example.block7crudvalidation.persona.domain.entity.Persona;
import com.example.block7crudvalidation.persona.domain.enums.Role;
import com.example.block7crudvalidation.profesor.domain.entity.Profesor;
import com.example.block7crudvalidation.profesor.infrastructure.repository.ProfesorRepository;
import com.example.block7crudvalidation.student.domain.entity.Student;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class ProfesorServiceImplTest {
    @Autowired
    private MockMvc mockMvc;
    @Mock
    private ProfesorRepository profesorRepository;

    @InjectMocks
    private ProfesorServiceImpl profesorService;
    @Test
    void crearProfesor() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024,5,20);
        Calendar c2 = Calendar.getInstance();
        c2.set(2050,5,20);
        Persona persona = new Persona(1,"juan123","1234","juan","lopez","juancompany@gmail.com","juanlopez@gmail.com","jodar",true, c1.getTime(),"http//:imagen.com",c2.getTime(), Role.USER,2,2);
        Asignatura asignatura = new Asignatura(1,"lengua","castellano", LocalDate.of(2024,5,10),LocalDate.of(2025,5,10));
        List<Asignatura> asignaturaList = new ArrayList<>();
        asignaturaList.add(asignatura);
        Student student = new Student(1,10,"bueno","ciencia",persona,asignaturaList,new Profesor());
        List<Student> studentList = new ArrayList<>();
        studentList.add(student);
        Profesor profesorSimulado = new Profesor(1,"bueno","back",persona,studentList);
        Profesor profesorEsperado = new Profesor(1,"bueno","back",persona,studentList);
        Mockito.when(profesorRepository.save(profesorSimulado)).thenReturn(profesorSimulado);
        final Profesor resultado = profesorService.crearProfesor(profesorSimulado);
        assertEquals(resultado.toString(), profesorEsperado.toString());
    }

    @Test
    void mostrarProfesores() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024,5,20);
        Calendar c2 = Calendar.getInstance();
        c2.set(2050,5,20);
        Persona persona = new Persona(1,"juan123","1234","juan","lopez","juancompany@gmail.com","juanlopez@gmail.com","jodar",true, c1.getTime(),"http//:imagen.com",c2.getTime(), Role.USER,2,2);
        Asignatura asignatura = new Asignatura(1,"lengua","castellano", LocalDate.of(2024,5,10),LocalDate.of(2025,5,10));
        List<Asignatura> asignaturaList = new ArrayList<>();
        asignaturaList.add(asignatura);
        Student student = new Student(1,10,"bueno","ciencia",persona,asignaturaList,new Profesor());
        List<Student> studentList = new ArrayList<>();
        studentList.add(student);
        Profesor profesorSimulado = new Profesor(1,"bueno","back",persona,studentList);
        List<Profesor> profesorsSimulado=new ArrayList<>();
        profesorsSimulado.add(profesorSimulado);
        Profesor profesorEsperado = new Profesor(1,"bueno","back",persona,studentList);
        List<Profesor> profesorsEsperado=new ArrayList<>();
        profesorsEsperado.add(profesorEsperado);
        Mockito.when(profesorRepository.findAll()).thenReturn(profesorsSimulado);
        final List<Profesor> resultado = profesorService.mostrarProfesores();
        assertEquals(resultado.toString(), profesorsEsperado.toString());
    }

    @Test
    void mostrarPorId() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024,5,20);
        Calendar c2 = Calendar.getInstance();
        c2.set(2050,5,20);
        Persona persona = new Persona(1,"juan123","1234","juan","lopez","juancompany@gmail.com","juanlopez@gmail.com","jodar",true, c1.getTime(),"http//:imagen.com",c2.getTime(), Role.USER,2,2);
        Asignatura asignatura = new Asignatura(1,"lengua","castellano", LocalDate.of(2024,5,10),LocalDate.of(2025,5,10));
        List<Asignatura> asignaturaList = new ArrayList<>();
        asignaturaList.add(asignatura);
        Student student = new Student(1,10,"bueno","ciencia",persona,asignaturaList,new Profesor());
        List<Student> studentList = new ArrayList<>();
        studentList.add(student);
        Profesor profesorSimulado = new Profesor(1,"bueno","back",persona,studentList);
        Profesor profesorEsperado = new Profesor(1,"bueno","back",persona,studentList);
        Mockito.when(profesorRepository.findById(1)).thenReturn(Optional.of(profesorSimulado));
        final Profesor resultado = profesorService.mostrarPorId(1);
        assertEquals(resultado.toString(), profesorEsperado.toString());
    }

    @Test
    void modificarProfesor() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024,5,20);
        Calendar c2 = Calendar.getInstance();
        c2.set(2050,5,20);
        Persona persona = new Persona(1,"juan123","1234","juan","lopez","juancompany@gmail.com","juanlopez@gmail.com","jodar",true, c1.getTime(),"http//:imagen.com",c2.getTime(), Role.USER,2,2);
        Asignatura asignatura = new Asignatura(1,"lengua","castellano", LocalDate.of(2024,5,10),LocalDate.of(2025,5,10));
        List<Asignatura> asignaturaList = new ArrayList<>();
        asignaturaList.add(asignatura);
        Student student = new Student(1,10,"bueno","ciencia",persona,asignaturaList,new Profesor());
        List<Student> studentList = new ArrayList<>();
        studentList.add(student);
        Profesor profesorSimulado = new Profesor(1,"bueno","back",persona,studentList);
        Profesor profesorEsperado = new Profesor(1,"malo","back",persona,studentList);
        Profesor profesorModificado = new Profesor(1,"malo","back",persona,studentList);
        Mockito.when(profesorRepository.findById(1)).thenReturn(Optional.of(profesorSimulado));
        final Profesor resultado = profesorService.modificarProfesor(1,profesorModificado);
        assertEquals(resultado.toString(), profesorEsperado.toString());
    }

    @Test
    void eliminarProfesor() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024,5,20);
        Calendar c2 = Calendar.getInstance();
        c2.set(2050,5,20);
        Persona persona = new Persona(1,"juan123","1234","juan","lopez","juancompany@gmail.com","juanlopez@gmail.com","jodar",true, c1.getTime(),"http//:imagen.com",c2.getTime(), Role.USER,2,2);
        Asignatura asignatura = new Asignatura(1,"lengua","castellano", LocalDate.of(2024,5,10),LocalDate.of(2025,5,10));
        List<Asignatura> asignaturaList = new ArrayList<>();
        asignaturaList.add(asignatura);
        Student student = new Student(1,10,"bueno","ciencia",persona,asignaturaList,new Profesor());
        List<Student> studentList = new ArrayList<>();
        studentList.add(student);
        Profesor profesorSimulado = new Profesor(1,"bueno","back",persona,studentList);
        Profesor profesorEsperado = new Profesor(1,"bueno","back",persona,studentList);
        Mockito.when(profesorRepository.findById(1)).thenReturn(Optional.of(profesorSimulado));
        final Profesor resultado = profesorService.eliminarProfesor(1);
        assertEquals(resultado.toString(), profesorEsperado.toString());
    }
}