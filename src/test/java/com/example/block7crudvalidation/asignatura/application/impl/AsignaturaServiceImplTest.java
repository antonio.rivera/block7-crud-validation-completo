package com.example.block7crudvalidation.asignatura.application.impl;

import com.example.block7crudvalidation.asignatura.domain.entity.Asignatura;
import com.example.block7crudvalidation.asignatura.infrastructure.repository.AsignaturaRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
@ExtendWith(MockitoExtension.class)
class AsignaturaServiceImplTest {
    @Autowired
    private MockMvc mockMvc;
    @Mock
    private AsignaturaRepository asignaturaRepository;

    @InjectMocks
    private AsignaturaServiceImpl asignaturaService;
    @Test
    void crearAsignatura() {
        Asignatura asignaturaSimulada = new Asignatura(1,"lengua","castellano", LocalDate.of(2024,5,10),LocalDate.of(2025,5,10));
        Asignatura asignaturaEsperada = new Asignatura(1,"lengua","castellano", LocalDate.of(2024,5,10),LocalDate.of(2025,5,10));;
        Mockito.when(asignaturaRepository.save(asignaturaSimulada)).thenReturn(asignaturaSimulada);
        final Asignatura resultado = asignaturaService.crearAsignatura(asignaturaSimulada);
        assertEquals(resultado.toString(), asignaturaEsperada.toString());
    }

    @Test
    void mostrarAsignaturas() {
        Asignatura asignatura1 = new Asignatura(1,"lengua","castellano", LocalDate.of(2024,5,10),LocalDate.of(2025,5,10));
        Asignatura asignatura2 = new Asignatura(2,"lengua","castellano", LocalDate.of(2024,5,10),LocalDate.of(2025,5,10));
        Asignatura asignatura3 = new Asignatura(3,"lengua","castellano", LocalDate.of(2024,5,10),LocalDate.of(2025,5,10));
        List<Asignatura> asignaturasSimuladas=new ArrayList<>();
        asignaturasSimuladas.add(asignatura1);
        asignaturasSimuladas.add(asignatura2);
        asignaturasSimuladas.add(asignatura3);
        Asignatura asignatura4 = new Asignatura(1,"lengua","castellano", LocalDate.of(2024,5,10),LocalDate.of(2025,5,10));
        Asignatura asignatura5 = new Asignatura(2,"lengua","castellano", LocalDate.of(2024,5,10),LocalDate.of(2025,5,10));
        Asignatura asignatura6 = new Asignatura(3,"lengua","castellano", LocalDate.of(2024,5,10),LocalDate.of(2025,5,10));
        List<Asignatura> asignaturasEsperadas=new ArrayList<>();
        asignaturasEsperadas.add(asignatura4);
        asignaturasEsperadas.add(asignatura5);
        asignaturasEsperadas.add(asignatura6);
        Mockito.when(asignaturaRepository.findAll()).thenReturn(asignaturasSimuladas);
        final List<Asignatura> resultado = asignaturaService.mostrarAsignaturas();
        assertEquals(resultado.toString(), asignaturasEsperadas.toString());

    }

    @Test
    void mostrarPorId() {
        Asignatura asignaturaSimulada = new Asignatura(1,"lengua","castellano", LocalDate.of(2024,5,10),LocalDate.of(2025,5,10));
        Asignatura asignaturaEsperada = new Asignatura(1,"lengua","castellano", LocalDate.of(2024,5,10),LocalDate.of(2025,5,10));;
        Mockito.when(asignaturaRepository.findById(1)).thenReturn(Optional.of(asignaturaSimulada));
        final Asignatura resultado = asignaturaService.mostrarPorId(1);
        assertEquals(resultado.toString(), asignaturaEsperada.toString());
    }

    @Test
    void modificarAsignatura() {
        Asignatura asignaturaSimulada = new Asignatura(1,"lengua","castellano", LocalDate.of(2024,5,10),LocalDate.of(2025,5,10));
        Asignatura asignaturaEsperada = new Asignatura(1,"lengua2","castellano", LocalDate.of(2024,5,10),LocalDate.of(2025,5,10));;
        Asignatura asignaturaModificada = new Asignatura(1,"lengua2","castellano", LocalDate.of(2024,5,10),LocalDate.of(2025,5,10));;
        Mockito.when(asignaturaRepository.findById(1)).thenReturn(Optional.of(asignaturaSimulada));
        final Asignatura resultado = asignaturaService.modificarAsignatura(1,asignaturaModificada);
        assertEquals(resultado.toString(), asignaturaEsperada.toString());
    }

    @Test
    void eliminarAsignatura() {
        Asignatura asignaturaSimulada = new Asignatura(1,"lengua","castellano", LocalDate.of(2024,5,10),LocalDate.of(2025,5,10));
        Asignatura asignaturaEsperada = new Asignatura(1,"lengua","castellano", LocalDate.of(2024,5,10),LocalDate.of(2025,5,10));;
        Mockito.when(asignaturaRepository.findById(1)).thenReturn(Optional.of(asignaturaSimulada));
        final Asignatura resultado = asignaturaService.eliminarAsignatura(1);
        assertEquals(resultado.toString(), asignaturaEsperada.toString());
    }
}