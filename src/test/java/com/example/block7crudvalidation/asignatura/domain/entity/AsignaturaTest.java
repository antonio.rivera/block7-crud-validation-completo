package com.example.block7crudvalidation.asignatura.domain.entity;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class AsignaturaTest {
    @Test
    void getSetId_asignatura() {
        Asignatura asignatura = new Asignatura();
        asignatura.setId_asignatura(1);
        assertEquals(asignatura.getId_asignatura(),1);
    }

    @Test
    void getSetAsignatura() {
        Asignatura asignatura = new Asignatura();
        asignatura.setAsignatura("lengua");
        assertEquals(asignatura.getAsignatura(),"lengua");
    }

    @Test
    void getSetComents() {
        Asignatura asignatura = new Asignatura();
        asignatura.setComents("castellano");
        assertEquals(asignatura.getComents(),"castellano");
    }

    @Test
    void getSetInitial_date() {
        Asignatura asignatura = new Asignatura();
        asignatura.setInitial_date( LocalDate.of(2024,5,10));
        assertEquals(asignatura.getInitial_date(), LocalDate.of(2024,5,10));
    }

    @Test
    void getSetFinish_date() {
        Asignatura asignatura = new Asignatura();
        asignatura.setFinish_date( LocalDate.of(2025,5,10));
        assertEquals(asignatura.getFinish_date(), LocalDate.of(2025,5,10));
    }

    @Test
    void testToString() {
        Asignatura asignatura = new Asignatura(1,"lengua","castellano", LocalDate.of(2024,5,10),LocalDate.of(2025,5,10));
        assertEquals(asignatura.toString(),"Asignatura(id_asignatura=1, asignatura=lengua, coments=castellano, initial_date=2024-05-10, finish_date=2025-05-10)");
    }
}