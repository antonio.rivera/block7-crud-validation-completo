package com.example.block7crudvalidation.persona.domain.entity;

import com.example.block7crudvalidation.persona.domain.enums.Role;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Calendar;

import static org.junit.jupiter.api.Assertions.*;

class PersonaTest {
    @Autowired
    private UserDetails userDetails;
    /*@Test
    void getAuthorities() {
        assertEquals(userDetails.getAuthorities(),"");

    }*/

    @Test
    void getUsername() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024,5,20);
        Calendar c2 = Calendar.getInstance();
        c2.set(2050,5,20);
        Persona persona = new Persona(1,"juan123","1234","juan","lopez","juancompany@gmail.com","juanlopez@gmail.com","jodar",true, c1.getTime(),"http//:imagen.com",c2.getTime(), Role.USER,2,2);
        assertEquals(persona.getUsuario(),persona.getUsername());
    }

    @Test
    void isAccountNonExpired() {
        Persona persona = new Persona();
        assertEquals(true, persona.isAccountNonExpired());
    }

    @Test
    void isAccountNonLocked() {
        Persona persona = new Persona();
        assertEquals(true,persona.isAccountNonLocked());
    }

    @Test
    void isCredentialsNonExpired() {
        Persona persona = new Persona();
        assertEquals(true,persona.isCredentialsNonExpired());
    }

    @Test
    void isEnabled() {
        Persona persona = new Persona();
        assertEquals(true,persona.isEnabled());

    }

    @Test
    void getSetSetId_persona() {
        Persona persona = new Persona();
        persona.setId_persona(1);
        assertEquals(1,persona.getId_persona());
    }

    @Test
    void getSetUsuario() {
        Persona persona = new Persona();
        persona.setUsuario("juan123");
        assertEquals("juan123",persona.getUsuario());
    }

    @Test
    void getSetPassword() {
        Persona persona = new Persona();
        persona.setPassword("1234");
        assertEquals("1234",persona.getPassword());
    }

    @Test
    void getSetName() {
        Persona persona = new Persona();
        persona.setName("juan");
        assertEquals("juan",persona.getName());
    }

    @Test
    void getSetSurname() {
        Persona persona = new Persona();
        persona.setSurname("lopez");
        assertEquals("lopez",persona.getSurname());
    }

    @Test
    void getSetCompany_email() {
        Persona persona = new Persona();
        persona.setCompany_email("juancompany@gmail.com");
        assertEquals("juancompany@gmail.com",persona.getCompany_email());
    }

    @Test
    void getSetPersonal_email() {
        Persona persona = new Persona();
        persona.setPersonal_email("juanjodar@gmail.com");
        assertEquals("juanjodar@gmail.com",persona.getPersonal_email());
    }

    @Test
    void getSetCity() {
        Persona persona = new Persona();
        persona.setCity("jodar");
        assertEquals("jodar",persona.getCity());
    }

    @Test
    void getSetActive() {
        Persona persona = new Persona();
        persona.setActive(true);
        assertEquals(true,persona.getActive());
    }

    @Test
    void getSetCreated_date() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024,5,20);
        Calendar c2 = Calendar.getInstance();
        c2.set(2024,5,20);
        Persona persona = new Persona();
        persona.setCreated_date(c1.getTime());
        assertEquals(persona.getCreated_date(),c2.getTime());
    }

    @Test
    void getSetImagen_url() {
        Persona persona = new Persona();
        persona.setImagen_url("http//:imagen.com");
        assertEquals("http//:imagen.com",persona.getImagen_url());
    }

    @Test
    void getSetTermination_date() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024,5,20);
        Calendar c2 = Calendar.getInstance();
        c2.set(2024,5,20);
        Persona persona = new Persona();
        persona.setTermination_date(c1.getTime());
        assertEquals(persona.getTermination_date().toString(),c2.getTime().toString());
    }

    @Test
    void getSetRole() {
        Persona persona = new Persona();
        persona.setRole(Role.USER);
        assertEquals(Role.USER,persona.getRole());
    }

    @Test
    void getSetStudent_id() {
        Persona persona = new Persona();
        persona.setStudent_id(1);
        assertEquals(1,persona.getStudent_id());
    }

    @Test
    void getSetProfesor_id() {
        Persona persona = new Persona();
        persona.setProfesor_id(1);
        assertEquals(1,persona.getProfesor_id());
    }


    @Test
    void testToString() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024,5,21);
        Calendar c2 = Calendar.getInstance();
        c2.set(2024,5,21);
        Persona persona = new Persona(1,"juan123","1234","juan","lopez","juancompany@gmail.com","juanlopez@gmail.com","jodar",true, c1.getTime(),"http//:imagen.com",c2.getTime(),Role.USER,2,2);
        assertEquals("Persona(id_persona=1, usuario=juan123, password=1234, name=juan, surname=lopez, company_email=juancompany@gmail.com, personal_email=juanlopez@gmail.com, city=jodar, active=true, created_date="+c1.getTime()+", imagen_url=http//:imagen.com, termination_date="+c2.getTime()+", role=USER, student_id=2, profesor_id=2)",persona.toString());
    }

    @Test
    void builder() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024,5,21);
        Calendar c2 = Calendar.getInstance();
        c2.set(2024,5,21);
        Persona personaEsperada = new Persona(1,"juan123","1234","juan","lopez","juancompany@gmail.com","juanlopez@gmail.com","jodar",true, c1.getTime(),"http//:imagen.com",c2.getTime(),Role.USER,2,2);

        Persona personaSimulada = Persona.builder()
                .id_persona(1)
                .usuario("juan123")
                .password("1234")
                .name("juan")
                .surname("lopez")
                .company_email("juancompany@gmail.com")
                .personal_email("juanlopez@gmail.com")
                .city("jodar")
                .active(true)
                .created_date(c1.getTime())
                .imagen_url("http//:imagen.com")
                .role(Role.USER)
                .termination_date(c2.getTime())
                .student_id(2)
                .profesor_id(2)
                .build();

        assertEquals(personaSimulada.toString(),personaEsperada.toString());


    }
}