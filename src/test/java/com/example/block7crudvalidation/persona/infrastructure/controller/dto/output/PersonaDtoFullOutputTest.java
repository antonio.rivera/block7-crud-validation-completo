package com.example.block7crudvalidation.persona.infrastructure.controller.dto.output;

import com.example.block7crudvalidation.asignatura.domain.entity.Asignatura;
import com.example.block7crudvalidation.persona.domain.entity.Persona;
import com.example.block7crudvalidation.persona.domain.enums.Role;
import com.example.block7crudvalidation.profesor.domain.entity.Profesor;
import com.example.block7crudvalidation.student.domain.entity.Student;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PersonaDtoFullOutputTest {
    @Test
    void getSetSetId_persona() {
        PersonaDtoFullOutput personaDtoFullOutput = new PersonaDtoFullOutput();
        personaDtoFullOutput.setId_persona(1);
        assertEquals(1,personaDtoFullOutput.getId_persona());
    }

    @Test
    void getSetUsuario() {
        PersonaDtoFullOutput peersonaDtoFullOutput = new PersonaDtoFullOutput();
        peersonaDtoFullOutput.setUsuario("juan123");
        assertEquals("juan123",peersonaDtoFullOutput.getUsuario());
    }

    @Test
    void getSetPassword() {
        PersonaDtoFullOutput peersonaDtoFullOutput = new PersonaDtoFullOutput();
        peersonaDtoFullOutput.setPassword("1234");
        assertEquals("1234",peersonaDtoFullOutput.getPassword());
    }

    @Test
    void getSetName() {
        PersonaDtoFullOutput peersonaDtoFullOutput = new PersonaDtoFullOutput();
        peersonaDtoFullOutput.setName("juan");
        assertEquals("juan",peersonaDtoFullOutput.getName());
    }

    @Test
    void getSetSurname() {
        PersonaDtoFullOutput peersonaDtoFullOutput = new PersonaDtoFullOutput();
        peersonaDtoFullOutput.setSurname("lopez");
        assertEquals("lopez",peersonaDtoFullOutput.getSurname());
    }

    @Test
    void getSetCompany_email() {
        PersonaDtoFullOutput peersonaDtoFullOutput = new PersonaDtoFullOutput();
        peersonaDtoFullOutput.setCompany_email("juancompany@gmail.com");
        assertEquals("juancompany@gmail.com",peersonaDtoFullOutput.getCompany_email());
    }

    @Test
    void getSetPersonal_email() {
        PersonaDtoFullOutput peersonaDtoFullOutput = new PersonaDtoFullOutput();
        peersonaDtoFullOutput.setPersonal_email("juanjodar@gmail.com");
        assertEquals("juanjodar@gmail.com",peersonaDtoFullOutput.getPersonal_email());
    }

    @Test
    void getSetCity() {
        PersonaDtoFullOutput peersonaDtoFullOutput = new PersonaDtoFullOutput();
        peersonaDtoFullOutput.setCity("jodar");
        assertEquals("jodar",peersonaDtoFullOutput.getCity());
    }

    @Test
    void getSetActive() {
        PersonaDtoFullOutput peersonaDtoFullOutput = new PersonaDtoFullOutput();
        peersonaDtoFullOutput.setActive(true);
        assertEquals(true,peersonaDtoFullOutput.getActive());
    }

    @Test
    void getSetCreated_date() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024, 5, 20);
        Calendar c2 = Calendar.getInstance();
        c2.set(2024, 5, 20);
        PersonaDtoFullOutput peersonaDtoFullOutput = new PersonaDtoFullOutput();
        peersonaDtoFullOutput.setCreated_date(c1.getTime());
        assertEquals(peersonaDtoFullOutput.getCreated_date().toString(), c2.getTime().toString());
    }

    @Test
    void getSetImagen_url() {
        PersonaDtoFullOutput peersonaDtoFullOutput = new PersonaDtoFullOutput();
        peersonaDtoFullOutput.setImagen_url("http//:imagen.com");
        assertEquals("http//:imagen.com",peersonaDtoFullOutput.getImagen_url());
    }

    @Test
    void getSetTermination_date() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024, 5, 20);
        Calendar c2 = Calendar.getInstance();
        c2.set(2024, 5, 20);
        PersonaDtoFullOutput peersonaDtoFullOutput = new PersonaDtoFullOutput();
        peersonaDtoFullOutput.setTermination_date(c1.getTime());
        assertEquals(peersonaDtoFullOutput.getTermination_date().toString(), c2.getTime().toString());
    }


    @Test
    void getStudent() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024, 5, 20);
        Calendar c2 = Calendar.getInstance();
        c2.set(2050, 5, 20);
        Persona persona = new Persona(1, "juan123", "1234", "juan", "lopez", "juancompany@gmail.com", "juanlopez@gmail.com", "jodar", true, c1.getTime(), "http//:imagen.com", c2.getTime(), Role.USER, 2, 2);
        Asignatura asignatura = new Asignatura(1, "lengua", "castellano", LocalDate.of(2024, 5, 10), LocalDate.of(2025, 5, 10));
        List<Asignatura> asignaturaList = new ArrayList<>();
        asignaturaList.add(asignatura);
        Student student = new Student(1, 10, "bueno", "ciencia", persona, asignaturaList, new Profesor());
        PersonaDtoFullOutput personaDtoFullOutput = new PersonaDtoFullOutput();
        personaDtoFullOutput.setStudent(student);
        assertEquals(personaDtoFullOutput.getStudent(), student);
    }

    @Test
    void getProfesor() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024, 5, 20);
        Calendar c2 = Calendar.getInstance();
        c2.set(2050, 5, 20);
        Persona persona = new Persona(1, "juan123", "1234", "juan", "lopez", "juancompany@gmail.com", "juanlopez@gmail.com", "jodar", true, c1.getTime(), "http//:imagen.com", c2.getTime(), Role.USER, 2, 2);
        Asignatura asignatura = new Asignatura(1, "lengua", "castellano", LocalDate.of(2024, 5, 10), LocalDate.of(2025, 5, 10));
        List<Asignatura> asignaturaList = new ArrayList<>();
        Student student = new Student(1, 10, "bueno", "ciencia", persona, asignaturaList, new Profesor());
        List<Student> studentList = new ArrayList<>();
        studentList.add(student);
        Profesor profesor = new Profesor(1, "bueno", "ciencia", persona, studentList);
        PersonaDtoFullOutput personaDtoFullOutput = new PersonaDtoFullOutput();
        personaDtoFullOutput.setProfesor(profesor);
        assertEquals(personaDtoFullOutput.getProfesor(), profesor);
    }
}