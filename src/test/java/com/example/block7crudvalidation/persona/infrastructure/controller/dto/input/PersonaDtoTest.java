package com.example.block7crudvalidation.persona.infrastructure.controller.dto.input;

import org.junit.jupiter.api.Test;

import java.util.Calendar;

import static org.junit.jupiter.api.Assertions.*;

class PersonaDtoTest {
    @Test
    void getSetSetId_persona() {
        PersonaDto personaDto = new PersonaDto();
        personaDto.setIdPersona(1);
        assertEquals(1, personaDto.getIdPersona());
    }

    @Test
    void getSetUsuario() {
        PersonaDto personaDto = new PersonaDto();
        personaDto.setUsuario("juan123");
        assertEquals("juan123",personaDto.getUsuario());
    }

    @Test
    void getSetPassword() {
        PersonaDto personaDto = new PersonaDto();
        personaDto.setPassword("1234");
        assertEquals("1234",personaDto.getPassword());
    }

    @Test
    void getSetName() {
        PersonaDto personaDto = new PersonaDto();
        personaDto.setName("juan");
        assertEquals("juan",personaDto.getName());
    }

    @Test
    void getSetSurname() {
        PersonaDto personaDto = new PersonaDto();
        personaDto.setSurname("lopez");
        assertEquals("lopez",personaDto.getSurname());
    }

    @Test
    void getSetCompany_email() {
        PersonaDto personaDto = new PersonaDto();
        personaDto.setCompanyEmail("juancompany@gmail.com");
        assertEquals("juancompany@gmail.com",personaDto.getCompanyEmail());
    }

    @Test
    void getSetPersonal_email() {
        PersonaDto personaDto = new PersonaDto();
        personaDto.setPersonalEmail("juanjodar@gmail.com");
        assertEquals("juanjodar@gmail.com",personaDto.getPersonalEmail());
    }

    @Test
    void getSetCity() {
        PersonaDto personaDto = new PersonaDto();
        personaDto.setCity("jodar");
        assertEquals("jodar",personaDto.getCity());
    }

    @Test
    void getSetActive() {
        PersonaDto personaDto = new PersonaDto();
        personaDto.setActive(true);
        assertEquals(true,personaDto.getActive());
    }

    @Test
    void getSetCreated_date() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024,5,20);
        Calendar c2 = Calendar.getInstance();
        c2.set(2024,5,20);
        PersonaDto personaDto = new PersonaDto();
        personaDto.setCreatedDate(c1.getTime());
        assertEquals(personaDto.getCreatedDate(),c2.getTime());
    }

    @Test
    void getSetImagen_url() {
        PersonaDto personaDto = new PersonaDto();
        personaDto.setImagenUrl("http//:imagen.com");
        assertEquals("http//:imagen.com",personaDto.getImagenUrl());
    }

    @Test
    void getSetTermination_date() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024,5,21);
        Calendar c2 = Calendar.getInstance();
        c2.set(2024,5,21);
        PersonaDto personaDto = new PersonaDto();
        personaDto.setTerminationDate(c1.getTime());
        assertEquals(personaDto.getTerminationDate().toString(),c2.getTime().toString());
    }



    @Test
    void getSetStudent_id() {
        PersonaDto personaDto = new PersonaDto();
        personaDto.setStudentId(1);
        assertEquals(1,personaDto.getStudentId());
    }

    @Test
    void getSetProfesor_id() {
        PersonaDto personaDto = new PersonaDto();
        personaDto.setProfesorId(1);
        assertEquals(1,personaDto.getProfesorId());
    }
}