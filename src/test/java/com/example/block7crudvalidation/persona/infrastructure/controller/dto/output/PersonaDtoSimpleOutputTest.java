package com.example.block7crudvalidation.persona.infrastructure.controller.dto.output;

import org.junit.jupiter.api.Test;

import java.util.Calendar;

import static org.junit.jupiter.api.Assertions.*;

class PersonaDtoSimpleOutputTest {
    @Test
    void getSetSetId_persona() {
        PersonaDtoSimpleOutput personaDtoSimpleOutput = new PersonaDtoSimpleOutput();
        personaDtoSimpleOutput.setId_persona(1);
        assertEquals(1,personaDtoSimpleOutput.getId_persona());
    }

    @Test
    void getSetUsuario() {
        PersonaDtoSimpleOutput personaDtoSimpleOutput = new PersonaDtoSimpleOutput();
        personaDtoSimpleOutput.setUsuario("juan123");
        assertEquals("juan123",personaDtoSimpleOutput.getUsuario());
    }

    @Test
    void getSetPassword() {
        PersonaDtoSimpleOutput personaDtoSimpleOutput = new PersonaDtoSimpleOutput();
        personaDtoSimpleOutput.setPassword("1234");
        assertEquals("1234",personaDtoSimpleOutput.getPassword());
    }

    @Test
    void getSetName() {
        PersonaDtoSimpleOutput personaDtoSimpleOutput = new PersonaDtoSimpleOutput();
        personaDtoSimpleOutput.setName("juan");
        assertEquals("juan",personaDtoSimpleOutput.getName());
    }

    @Test
    void getSetSurname() {
        PersonaDtoSimpleOutput personaDtoSimpleOutput = new PersonaDtoSimpleOutput();
        personaDtoSimpleOutput.setSurname("lopez");
        assertEquals("lopez",personaDtoSimpleOutput.getSurname());
    }

    @Test
    void getSetCompany_email() {
        PersonaDtoSimpleOutput personaDtoSimpleOutput = new PersonaDtoSimpleOutput();
        personaDtoSimpleOutput.setCompany_email("juancompany@gmail.com");
        assertEquals("juancompany@gmail.com",personaDtoSimpleOutput.getCompany_email());
    }

    @Test
    void getSetPersonal_email() {
        PersonaDtoSimpleOutput personaDtoSimpleOutput = new PersonaDtoSimpleOutput();
        personaDtoSimpleOutput.setPersonal_email("juanjodar@gmail.com");
        assertEquals("juanjodar@gmail.com",personaDtoSimpleOutput.getPersonal_email());
    }

    @Test
    void getSetCity() {
        PersonaDtoSimpleOutput personaDtoSimpleOutput = new PersonaDtoSimpleOutput();
        personaDtoSimpleOutput.setCity("jodar");
        assertEquals("jodar",personaDtoSimpleOutput.getCity());
    }

    @Test
    void getSetActive() {
        PersonaDtoSimpleOutput personaDtoSimpleOutput = new PersonaDtoSimpleOutput();
        personaDtoSimpleOutput.setActive(true);
        assertEquals(true,personaDtoSimpleOutput.getActive());
    }

    @Test
    void getSetCreated_date() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024,5,20);
        Calendar c2 = Calendar.getInstance();
        c2.set(2024,5,20);
        PersonaDtoSimpleOutput personaDtoSimpleOutput = new PersonaDtoSimpleOutput();
        personaDtoSimpleOutput.setCreated_date(c1.getTime());
        assertEquals(personaDtoSimpleOutput.getCreated_date().toString(),c2.getTime().toString());
    }

    @Test
    void getSetImagen_url() {
        PersonaDtoSimpleOutput personaDtoSimpleOutput = new PersonaDtoSimpleOutput();
        personaDtoSimpleOutput.setImagen_url("http//:imagen.com");
        assertEquals("http//:imagen.com",personaDtoSimpleOutput.getImagen_url());
    }

    @Test
    void getSetTermination_date() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024,5,20);
        Calendar c2 = Calendar.getInstance();
        c2.set(2024,5,20);
        PersonaDtoSimpleOutput personaDtoSimpleOutput = new PersonaDtoSimpleOutput();
        personaDtoSimpleOutput.setTermination_date(c1.getTime());
        assertEquals(personaDtoSimpleOutput.getTermination_date().toString(),c2.getTime().toString());
    }

}