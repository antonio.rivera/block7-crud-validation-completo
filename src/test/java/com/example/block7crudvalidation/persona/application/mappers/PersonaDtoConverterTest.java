package com.example.block7crudvalidation.persona.application.mappers;

import com.example.block7crudvalidation.persona.domain.entity.Persona;
import com.example.block7crudvalidation.persona.domain.enums.Role;
import com.example.block7crudvalidation.persona.infrastructure.controller.dto.input.PersonaDto;
import com.example.block7crudvalidation.persona.infrastructure.controller.dto.output.PersonaDtoFullOutput;
import com.example.block7crudvalidation.persona.infrastructure.controller.dto.output.PersonaDtoSimpleOutput;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Calendar;

import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest
class PersonaDtoConverterTest {
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private PersonaDtoConverter personaDtoConverter;
    @Test
    void converToDto() {
        // En la funcion entra Persona y sale PersonaDto
        Calendar c1 = Calendar.getInstance();
        c1.set(2024,5,20);
        Calendar c2 = Calendar.getInstance();
        c2.set(2050,5,20);
        Persona persona = new Persona(1,"juan123","1234","juan","lopez","juancompany@gmail.com","juanlopez@gmail.com","jodar",true, c1.getTime(),"http//:imagen.com",c2.getTime(), Role.USER,2,2);
        //PersonaDto personaDto = new PersonaDto(1,"juan123","1234","juan","lopez","juancompany@gmail.com","juanlopez@gmail.com","jodar",true, c1.getTime(),"http//:imagen.com",c2.getTime(),2,null);
        PersonaDto personaDto = modelMapper.map(persona,PersonaDto.class);
        PersonaDto personaDto1 = personaDtoConverter.converToDto(persona);
        assertEquals(personaDto.toString(),personaDto1.toString());

    }

    @Test
    void simpleOutput() {
        // En la funcion entra Persona y sale PersonaDtoSimpleOutput
        Calendar c1 = Calendar.getInstance();
        c1.set(2024,5,20);
        Calendar c2 = Calendar.getInstance();
        c2.set(2050,5,20);
        Persona persona = new Persona(1,"juan123","1234","juan","lopez","juancompany@gmail.com","juanlopez@gmail.com","jodar",true, c1.getTime(),"http//:imagen.com",c2.getTime(), Role.USER,2,2);
        //PersonaDto personaDto = new PersonaDto(1,"juan123","1234","juan","lopez","juancompany@gmail.com","juanlopez@gmail.com","jodar",true, c1.getTime(),"http//:imagen.com",c2.getTime(),2,null);
        PersonaDtoSimpleOutput personaDto = modelMapper.map(persona, PersonaDtoSimpleOutput.class);
        PersonaDtoSimpleOutput personaDto1 = personaDtoConverter.simpleOutput(persona);
        assertEquals(personaDto.toString(),personaDto1.toString());
    }

    @Test
    void fullOutput() {
        // En la funcion entra Persona y sale PersonaDtoFullOutput
        Calendar c1 = Calendar.getInstance();
        c1.set(2024,5,20);
        Calendar c2 = Calendar.getInstance();
        c2.set(2050,5,20);
        Persona persona = new Persona(1,"juan123","1234","juan","lopez","juancompany@gmail.com","juanlopez@gmail.com","jodar",true, c1.getTime(),"http//:imagen.com",c2.getTime(), Role.USER,2,2);
        //PersonaDto personaDto = new PersonaDto(1,"juan123","1234","juan","lopez","juancompany@gmail.com","juanlopez@gmail.com","jodar",true, c1.getTime(),"http//:imagen.com",c2.getTime(),2,null);
        PersonaDtoFullOutput personaDto = modelMapper.map(persona, PersonaDtoFullOutput.class);
        PersonaDtoFullOutput personaDto1 = personaDtoConverter.fullOutput(persona);
        assertEquals(personaDto.toString(),personaDto1.toString());
    }
}