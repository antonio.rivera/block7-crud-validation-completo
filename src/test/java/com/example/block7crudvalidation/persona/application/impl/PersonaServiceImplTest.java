package com.example.block7crudvalidation.persona.application.impl;

import com.example.block7crudvalidation.persona.domain.entity.Persona;
import com.example.block7crudvalidation.persona.domain.enums.Role;
import com.example.block7crudvalidation.persona.infrastructure.repository.PersonaRepository;
import com.example.block7crudvalidation.z_shared.exception.UnprocessableEntityException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class PersonaServiceImplTest {
    @Autowired
    private MockMvc mockMvc;
    @Mock
    private PersonaRepository personaRepository;

    @InjectMocks
    private PersonaServiceImpl personaService;

    @Test
    void findPersonaById() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024, 5, 22);
        Calendar c2 = Calendar.getInstance();
        c2.set(2024, 5, 22);
        Persona personaSimulada = new Persona(1, "juan123", "1234", "juan", "lopez", "juancompany@gmail.com", "juanlopez@gmail.com", "jodar", true, c1.getTime(), "http//:imagen.com", c2.getTime(), Role.USER, 2, 2);
        Persona personaEsperada = new Persona(1, "juan123", "1234", "juan", "lopez", "juancompany@gmail.com", "juanlopez@gmail.com", "jodar", true, c1.getTime(), "http//:imagen.com", c2.getTime(), Role.USER, 2, 2);
        Mockito.when(personaRepository.findById(1)).thenReturn(Optional.of(personaSimulada));
        final Persona resultado = personaService.findPersonaById(1);
        assertEquals(resultado.toString(), personaEsperada.toString());
    }

    @Test
    void findByCampoUsuario() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024, 5, 22);
        Calendar c2 = Calendar.getInstance();
        c2.set(2024, 5, 22);
        Persona personaSimulada = new Persona(1, "juan123", "1234", "juan", "lopez", "juancompany@gmail.com", "juanlopez@gmail.com", "jodar", true, c1.getTime(), "http//:imagen.com", c2.getTime(), Role.USER, 2, 2);
        Persona personaEsperada = new Persona(1, "juan123", "1234", "juan", "lopez", "juancompany@gmail.com", "juanlopez@gmail.com", "jodar", true, c1.getTime(), "http//:imagen.com", c2.getTime(), Role.USER, 2, 2);
        Mockito.when(personaRepository.findByUsuario("juan123")).thenReturn(Optional.of(personaSimulada));
        final Persona resultado = personaService.findByCampoUsuario("juan123");
        assertEquals(resultado.toString(), personaEsperada.toString());
    }

    @Test
    void mostrarPersonas() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024, 5, 22);
        Calendar c2 = Calendar.getInstance();
        c2.set(2024, 5, 22);
        Persona persona1 = new Persona(1, "juan123", "1234", "juan", "lopez", "juancompany@gmail.com", "juanlopez@gmail.com", "jodar", true, c1.getTime(), "http//:imagen.com", c2.getTime(), Role.USER, 2, 2);
        Persona persona2 = new Persona(2, "juan322", "1234", "juan", "lopez", "juancompany@gmail.com", "juanlopez@gmail.com", "jodar", true, c1.getTime(), "http//:imagen.com", c2.getTime(), Role.USER, 3, 2);
        List<Persona> personaListSimulada = new ArrayList<>();
        personaListSimulada.add(persona1);
        personaListSimulada.add(persona2);
        Persona persona3 = new Persona(1, "juan123", "1234", "juan", "lopez", "juancompany@gmail.com", "juanlopez@gmail.com", "jodar", true, c1.getTime(), "http//:imagen.com", c2.getTime(), Role.USER, 2, 2);
        Persona persona4 = new Persona(2, "juan322", "1234", "juan", "lopez", "juancompany@gmail.com", "juanlopez@gmail.com", "jodar", true, c1.getTime(), "http//:imagen.com", c2.getTime(), Role.USER, 3, 2);
        List<Persona> personaListEsperada = new ArrayList<>();
        personaListEsperada.add(persona3);
        personaListEsperada.add(persona4);

        Mockito.when(personaRepository.findAll()).thenReturn(personaListSimulada);
        final List<Persona> resultado = personaService.mostrarPersonas();
        assertEquals(resultado.toString(), personaListEsperada.toString());
    }

    @Test
    void crearPersona() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024, 5, 22);
        Calendar c2 = Calendar.getInstance();
        c2.set(2024, 5, 22);
        Persona personaSimulada = new Persona(1, "juan123", "1234", "juan", "lopez", "juancompany@gmail.com", "juanlopez@gmail.com", "jodar", true, c1.getTime(), "http//:imagen.com", c2.getTime(), Role.USER, 2, 2);
        Persona personaEsperada = new Persona(1, "juan123", "1234", "juan", "lopez", "juancompany@gmail.com", "juanlopez@gmail.com", "jodar", true, c1.getTime(), "http//:imagen.com", c2.getTime(), Role.USER, 2, 2);
        Mockito.when(personaRepository.save(personaSimulada)).thenReturn(personaSimulada);
        final Persona resultado = personaService.crearPersona(personaSimulada);
        assertEquals(resultado.toString(), personaEsperada.toString());
    }

    @Test
    void modificarStudentPersona() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024, 5, 22);
        Calendar c2 = Calendar.getInstance();
        c2.set(2024, 5, 22);
        Persona personaSimulada = new Persona(1, "juan123", "1234", "juan", "lopez", "juancompany@gmail.com", "juanlopez@gmail.com", "jodar", true, c1.getTime(), "http//:imagen.com", c2.getTime(), Role.USER, 2, 2);
        Persona personaEsperada = new Persona(1, "juan123", "1234", "juan", "lopez", "juancompany@gmail.com", "juanlopez@gmail.com", "jodar", true, c1.getTime(), "http//:imagen.com", c2.getTime(), Role.USER, 3, 2);
        Persona personaModificada = new Persona(1, "juan123", "1234", "juan", "lopez", "juancompany@gmail.com", "juanlopez@gmail.com", "jodar", true, c1.getTime(), "http//:imagen.com", c2.getTime(), Role.USER, 3, 2);
        Mockito.when(personaRepository.findById(1)).thenReturn(Optional.of(personaSimulada));
        final Persona resultado = personaService.modificarStudentPersona(personaModificada);
        assertEquals(resultado.toString(), personaEsperada.toString());
    }

    @Test
    void eliminarPersona() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024, 5, 22);
        Calendar c2 = Calendar.getInstance();
        c2.set(2024, 5, 22);
        Persona personaSimulada = new Persona(1, "juan123", "1234", "juan", "lopez", "juancompany@gmail.com", "juanlopez@gmail.com", "jodar", true, c1.getTime(), "http//:imagen.com", c2.getTime(), Role.USER, null, null);
        Persona personaEsperada = new Persona(1, "juan123", "1234", "juan", "lopez", "juancompany@gmail.com", "juanlopez@gmail.com", "jodar", true, c1.getTime(), "http//:imagen.com", c2.getTime(), Role.USER, null, null);
        Mockito.when(personaRepository.findById(1)).thenReturn(Optional.of(personaSimulada));
        final Persona resultado = personaService.eliminarPersona(1);
        assertEquals(resultado.toString(), personaEsperada.toString());
    }

    @Test
    void camposOkUser() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024, 5, 22);
        Calendar c2 = Calendar.getInstance();
        c2.set(2024, 5, 22);
        Persona personaSimulada = new Persona(1, "", "1234", "juan", "lopez", "juancompany@gmail.com", "juanlopez@gmail.com", "jodar", true, c1.getTime(), "http//:imagen.com", c2.getTime(), Role.USER, null, null);
        assertThrows(UnprocessableEntityException.class, () -> {
            personaService.camposOk(personaSimulada);
        });
    }

    @Test
    void camposOkUser2() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024, 5, 22);
        Calendar c2 = Calendar.getInstance();
        c2.set(2024, 5, 22);
        Persona personaSimulada = new Persona(1, "e", "1234", "juan", "lopez", "juancompany@gmail.com", "juanlopez@gmail.com", "jodar", true, c1.getTime(), "http//:imagen.com", c2.getTime(), Role.USER, null, null);
        assertThrows(UnprocessableEntityException.class, () -> {
            personaService.camposOk(personaSimulada);
        });
    }

    @Test
    void camposOkPass() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024, 5, 22);
        Calendar c2 = Calendar.getInstance();
        c2.set(2024, 5, 22);
        Persona personaSimulada = new Persona(1, "juan123", "", "juan", "lopez", "juancompany@gmail.com", "juanlopez@gmail.com", "jodar", true, c1.getTime(), "http//:imagen.com", c2.getTime(), Role.USER, null, null);
        assertThrows(UnprocessableEntityException.class, () -> {
            personaService.camposOk(personaSimulada);
        });
    }

    @Test
    void camposOkName() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024, 5, 22);
        Calendar c2 = Calendar.getInstance();
        c2.set(2024, 5, 22);
        Persona personaSimulada = new Persona(1, "juan123", "1234", "", "lopez", "juancompany@gmail.com", "juanlopez@gmail.com", "jodar", true, c1.getTime(), "http//:imagen.com", c2.getTime(), Role.USER, null, null);
        assertThrows(UnprocessableEntityException.class, () -> {
            personaService.camposOk(personaSimulada);
        });
    }

    @Test
    void camposOkCompany_email() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024, 5, 22);
        Calendar c2 = Calendar.getInstance();
        c2.set(2024, 5, 22);
        Persona personaSimulada = new Persona(1, "juan123", "1234", "juan", "lopez", "", "juanlopez@gmail.com", "jodar", true, c1.getTime(), "http//:imagen.com", c2.getTime(), Role.USER, null, null);
        assertThrows(UnprocessableEntityException.class, () -> {
            personaService.camposOk(personaSimulada);
        });
    }

    @Test
    void camposOkPersonalEmail() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024, 5, 22);
        Calendar c2 = Calendar.getInstance();
        c2.set(2024, 5, 22);
        Persona personaSimulada = new Persona(1, "juan123", "1234", "juan", "lopez", "juancompany@gmail.com", "", "jodar", true, c1.getTime(), "http//:imagen.com", c2.getTime(), Role.USER, null, null);
        assertThrows(UnprocessableEntityException.class, () -> {
            personaService.camposOk(personaSimulada);
        });
    }

    @Test
    void camposOkCity() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024, 5, 22);
        Calendar c2 = Calendar.getInstance();
        c2.set(2024, 5, 22);
        Persona personaSimulada = new Persona(1, "juan123", "1234", "juan", "lopez", "juancompany@gmail.com", "juanlopez@gmail.com", "", true, c1.getTime(), "http//:imagen.com", c2.getTime(), Role.USER, null, null);
        assertThrows(UnprocessableEntityException.class, () -> {
            personaService.camposOk(personaSimulada);
        });
    }

    @Test
    void camposOkActive() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024, 5, 22);
        Calendar c2 = Calendar.getInstance();
        c2.set(2024, 5, 22);
        Persona personaSimulada = new Persona(1, "juan123", "1234", "juan", "lopez", "juancompany@gmail.com", "juanlopez@gmail.com", "jodar", null, c1.getTime(), "http//:imagen.com", c2.getTime(), Role.USER, null, null);
        assertThrows(UnprocessableEntityException.class, () -> {
            personaService.camposOk(personaSimulada);
        });
    }

    @Test
    void camposOkCreated_date() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024, 5, 22);
        Calendar c2 = Calendar.getInstance();
        c2.set(2024, 5, 22);
        Persona personaSimulada = new Persona(1, "juan123", "1234", "juan", "lopez", "juancompany@gmail.com", "juanlopez@gmail.com", "jodar", true, null, "http//:imagen.com", c2.getTime(), Role.USER, null, null);
        assertThrows(UnprocessableEntityException.class, () -> {
            personaService.camposOk(personaSimulada);
        });
    }
}