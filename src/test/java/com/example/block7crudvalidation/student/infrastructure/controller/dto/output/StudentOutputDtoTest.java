package com.example.block7crudvalidation.student.infrastructure.controller.dto.output;

import com.example.block7crudvalidation.persona.domain.entity.Persona;
import com.example.block7crudvalidation.persona.domain.enums.Role;
import org.junit.jupiter.api.Test;

import java.util.Calendar;

import static org.junit.jupiter.api.Assertions.*;

class StudentOutputDtoTest {
    @Test
    void getSetId_studentOutputDto() {
        StudentOutputDto studentOutputDto = new StudentOutputDto();
        studentOutputDto.setId_student(1);
        assertEquals(1,studentOutputDto.getId_student());
    }

    @Test
    void getSetNum_hours_week() {
        StudentOutputDto studentOutputDto = new StudentOutputDto();
        studentOutputDto.setNum_hours_week(10);
        assertEquals(10,studentOutputDto.getNum_hours_week());
    }

    @Test
    void getSetComents() {
        StudentOutputDto studentOutputDto = new StudentOutputDto();
        studentOutputDto.setComents("bueno");
        assertEquals("bueno",studentOutputDto.getComents());
    }

    @Test
    void getSetBranch() {
        StudentOutputDto studentOutputDto = new StudentOutputDto();
        studentOutputDto.setBranch("ciencia");
        assertEquals("ciencia",studentOutputDto.getBranch());
    }

    @Test
    void getSetPersona() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024,5,20);
        Calendar c2 = Calendar.getInstance();
        c2.set(2050,5,20);
        Persona persona = new Persona(1,"juan123","1234","juan","lopez","juancompany@gmail.com","juanlopez@gmail.com","jodar",true, c1.getTime(),"http//:imagen.com",c2.getTime(), Role.USER,2,2);
        StudentOutputDto studentOutputDto = new StudentOutputDto();
        studentOutputDto.setPersona(persona);
        assertEquals(persona,studentOutputDto.getPersona());
    }

    @Test
    void getSetId_persona() {
        StudentOutputDto studentOutputDto = new StudentOutputDto();
        studentOutputDto.setId_persona(1);
        assertEquals(1,studentOutputDto.getId_persona());
    }
}