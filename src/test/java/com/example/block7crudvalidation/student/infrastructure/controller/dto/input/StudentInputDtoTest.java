package com.example.block7crudvalidation.student.infrastructure.controller.dto.input;

import com.example.block7crudvalidation.persona.domain.entity.Persona;
import com.example.block7crudvalidation.persona.domain.enums.Role;
import org.junit.jupiter.api.Test;

import java.util.Calendar;

import static org.junit.jupiter.api.Assertions.*;

class StudentInputDtoTest {
    @Test
    void getSetId_studentInputDto() {
        StudentInputDto studentInputDto = new StudentInputDto();
        studentInputDto.setId_student(1);
        assertEquals(1,studentInputDto.getId_student());
    }

    @Test
    void getSetNum_hours_week() {
        StudentInputDto studentInputDto = new StudentInputDto();
        studentInputDto.setNum_hours_week(10);
        assertEquals(10,studentInputDto.getNum_hours_week());
    }

    @Test
    void getSetComents() {
        StudentInputDto studentInputDto = new StudentInputDto();
        studentInputDto.setComents("bueno");
        assertEquals("bueno",studentInputDto.getComents());
    }

    @Test
    void getSetBranch() {
        StudentInputDto studentInputDto = new StudentInputDto();
        studentInputDto.setBranch("ciencia");
        assertEquals("ciencia",studentInputDto.getBranch());
    }

    @Test
    void getSetPersona() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024,5,20);
        Calendar c2 = Calendar.getInstance();
        c2.set(2050,5,20);
        Persona persona = new Persona(1,"juan123","1234","juan","lopez","juancompany@gmail.com","juanlopez@gmail.com","jodar",true, c1.getTime(),"http//:imagen.com",c2.getTime(), Role.USER,2,2);
        StudentInputDto studentInputDto = new StudentInputDto();
        studentInputDto.setPersona(persona);
        assertEquals(persona,studentInputDto.getPersona());
    }

    @Test
    void getSetId_persona() {
        StudentInputDto studentInputDto = new StudentInputDto();
        studentInputDto.setId_persona(1);
        assertEquals(1,studentInputDto.getId_persona());
    }
}