package com.example.block7crudvalidation.student.application.impl;

import com.example.block7crudvalidation.asignatura.application.impl.AsignaturaServiceImpl;
import com.example.block7crudvalidation.asignatura.domain.entity.Asignatura;
import com.example.block7crudvalidation.asignatura.infrastructure.repository.AsignaturaRepository;
import com.example.block7crudvalidation.persona.domain.entity.Persona;
import com.example.block7crudvalidation.persona.domain.enums.Role;
import com.example.block7crudvalidation.profesor.domain.entity.Profesor;
import com.example.block7crudvalidation.student.domain.entity.Student;
import com.example.block7crudvalidation.student.infrastructure.repository.StudentRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class StudentServiceImplTest {

    @Autowired
    private MockMvc mockMvc;
    @Mock
    private StudentRepository studentRepository;

    @InjectMocks
    private StudentServiceImpl studentService;
    @Mock
    private AsignaturaRepository asignaturaRepository;

    @InjectMocks
    private AsignaturaServiceImpl asignaturaService;

    @Test
    void crearStudent() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024,5,20);
        Calendar c2 = Calendar.getInstance();
        c2.set(2050,5,20);
        Persona persona = new Persona(1,"juan123","1234","juan","lopez","juancompany@gmail.com","juanlopez@gmail.com","jodar",true, c1.getTime(),"http//:imagen.com",c2.getTime(), Role.USER,2,2);
        Asignatura asignatura = new Asignatura(1,"lengua","castellano", LocalDate.of(2024,5,10),LocalDate.of(2025,5,10));
        List<Asignatura> asignaturaList = new ArrayList<>();
        asignaturaList.add(asignatura);
        Student studentSimulado = new Student(1,10,"bueno","ciencia",persona,asignaturaList,new Profesor());
        Student studentEsperado = new Student(1,10,"bueno","ciencia",persona,asignaturaList,new Profesor());
        Mockito.when(studentRepository.save(studentSimulado)).thenReturn(studentSimulado);
        final Student resultado = studentService.crearStudent(studentSimulado);
        assertEquals(resultado.toString(),studentEsperado.toString());
    }

    @Test
    void mostrarStudents() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024,5,20);
        Calendar c2 = Calendar.getInstance();
        c2.set(2050,5,20);
        Persona persona = new Persona(1,"juan123","1234","juan","lopez","juancompany@gmail.com","juanlopez@gmail.com","jodar",true, c1.getTime(),"http//:imagen.com",c2.getTime(), Role.USER,1,null);
        Asignatura asignatura = new Asignatura(1,"lengua","castellano", LocalDate.of(2024,5,10),LocalDate.of(2025,5,10));
        List<Asignatura> asignaturaList = new ArrayList<>();
        asignaturaList.add(asignatura);
        Student studentSimulado = new Student(1,10,"bueno","ciencia",persona,asignaturaList,new Profesor());
        List<Student> studentsSimulado = new ArrayList<>();
        studentsSimulado.add(studentSimulado);
        Student studentEsperado = new Student(1,10,"bueno","ciencia",persona,asignaturaList,new Profesor());
        List<Student> studentsEsperado = new ArrayList<>();
        studentsEsperado.add(studentEsperado);
        Mockito.when(studentRepository.findAll()).thenReturn(studentsSimulado);
        final List<Student> resultado = studentService.mostrarStudents();
        assertEquals(resultado.toString(),studentsEsperado.toString());
    }

    @Test
    void mostrarPorId() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024,5,20);
        Calendar c2 = Calendar.getInstance();
        c2.set(2050,5,20);
        Persona persona = new Persona(1,"juan123","1234","juan","lopez","juancompany@gmail.com","juanlopez@gmail.com","jodar",true, c1.getTime(),"http//:imagen.com",c2.getTime(), Role.USER,2,2);
        Asignatura asignatura = new Asignatura(1,"lengua","castellano", LocalDate.of(2024,5,10),LocalDate.of(2025,5,10));
        List<Asignatura> asignaturaList = new ArrayList<>();
        asignaturaList.add(asignatura);
        Student studentSimulado = new Student(1,10,"bueno","ciencia",persona,asignaturaList,new Profesor());
        Student studentEsperado = new Student(1,10,"bueno","ciencia",persona,asignaturaList,new Profesor());
        Mockito.when(studentRepository.findById(1)).thenReturn(Optional.of(studentSimulado));
        final Student resultado = studentService.mostrarPorId(1);
        assertEquals(resultado.toString(),studentEsperado.toString());
    }

    @Test
    void modificarStudent() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024,5,20);
        Calendar c2 = Calendar.getInstance();
        c2.set(2050,5,20);
        Persona persona = new Persona(1,"juan123","1234","juan","lopez","juancompany@gmail.com","juanlopez@gmail.com","jodar",true, c1.getTime(),"http//:imagen.com",c2.getTime(), Role.USER,2,2);
        Asignatura asignatura = new Asignatura(1,"lengua","castellano", LocalDate.of(2024,5,10),LocalDate.of(2025,5,10));
        List<Asignatura> asignaturaList = new ArrayList<>();
        asignaturaList.add(asignatura);
        Student studentSimulado = new Student(1,10,"bueno","ciencia",persona,asignaturaList,new Profesor());
        Student studentEsperado = new Student(1,11,"bueno","ciencia",persona,asignaturaList,new Profesor());
        Student studentModificado = new Student(1,11,"bueno","ciencia",persona,asignaturaList,new Profesor());
        Mockito.when(studentRepository.findById(1)).thenReturn(Optional.of(studentSimulado));
        final Student resultado = studentService.modificarStudent(1,studentModificado);
        assertEquals(resultado.toString(),studentEsperado.toString());
    }

    @Test
    void eliminarStudent() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024,5,20);
        Calendar c2 = Calendar.getInstance();
        c2.set(2050,5,20);
        Persona persona = new Persona(1,"juan123","1234","juan","lopez","juancompany@gmail.com","juanlopez@gmail.com","jodar",true, c1.getTime(),"http//:imagen.com",c2.getTime(), Role.USER,2,2);
        Asignatura asignatura = new Asignatura(1,"lengua","castellano", LocalDate.of(2024,5,10),LocalDate.of(2025,5,10));
        List<Asignatura> asignaturaList = new ArrayList<>();
        asignaturaList.add(asignatura);
        Student studentSimulado = new Student(1,10,"bueno","ciencia",persona,asignaturaList,new Profesor());
        Student studentEsperado = new Student(1,10,"bueno","ciencia",persona,asignaturaList,new Profesor());
        Mockito.when(studentRepository.findById(1)).thenReturn(Optional.of(studentSimulado));
        final Student resultado = studentService.eliminarStudent(1);
        assertEquals(resultado.toString(),studentEsperado.toString());
    }

    @Test
    void estudianteAsignaturas() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024,5,20);
        Calendar c2 = Calendar.getInstance();
        c2.set(2050,5,20);
        Persona persona = new Persona(1,"juan123","1234","juan","lopez","juancompany@gmail.com","juanlopez@gmail.com","jodar",true, c1.getTime(),"http//:imagen.com",c2.getTime(), Role.USER,2,2);
        Asignatura asignatura = new Asignatura(1,"lengua","castellano", LocalDate.of(2024,5,10),LocalDate.of(2025,5,10));
        List<Asignatura> asignaturaList = new ArrayList<>();
        asignaturaList.add(asignatura);
        Student studentSimulado = new Student(1,10,"bueno","ciencia",persona,asignaturaList,new Profesor());
        //Student studentEsperado = new Student(1,10,"bueno","ciencia",persona,asignaturaList,new Profesor());
        Mockito.when(studentRepository.findById(1)).thenReturn(Optional.of(studentSimulado));
        final List<Asignatura> resultado = studentService.estudianteAsignaturas(1);
        assertEquals(resultado.toString(),asignaturaList.toString());
    }


}