package com.example.block7crudvalidation.student.application.mappers;

import com.example.block7crudvalidation.asignatura.domain.entity.Asignatura;
import com.example.block7crudvalidation.persona.domain.entity.Persona;
import com.example.block7crudvalidation.persona.domain.enums.Role;
import com.example.block7crudvalidation.profesor.domain.entity.Profesor;
import com.example.block7crudvalidation.student.domain.entity.Student;
import com.example.block7crudvalidation.student.infrastructure.controller.dto.input.StudentInputDto;
import com.example.block7crudvalidation.student.infrastructure.controller.dto.output.StudentOutputDto;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class StudentDtoConverterTest {
    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private StudentDtoConverter studentDtoConverter;

    @Test
    void converToInputStudent() {

        Calendar c1 = Calendar.getInstance();
        c1.set(2024,5,20);
        Calendar c2 = Calendar.getInstance();
        c2.set(2050,5,20);
        Persona persona = new Persona(1,"juan123","1234","juan","lopez","juancompany@gmail.com","juanlopez@gmail.com","jodar",true, c1.getTime(),"http//:imagen.com",c2.getTime(), Role.USER,2,2);
        Asignatura asignatura = new Asignatura(1,"lengua","castellano", LocalDate.of(2024,5,10),LocalDate.of(2025,5,10));
        List<Asignatura> asignaturaList = new ArrayList<>();
        asignaturaList.add(asignatura);
        StudentInputDto studentInputDto = new StudentInputDto(1,10,"bueno","ciencia",1,persona);

        Student student = modelMapper.map(studentInputDto,Student.class);
        Student student1 = studentDtoConverter.converToInputStudent(studentInputDto);

        assertEquals(student.toString(),student1.toString());

    }

    @Test
    void converToSimpleOutput() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024,5,20);
        Calendar c2 = Calendar.getInstance();
        c2.set(2050,5,20);
        Persona persona = new Persona(1,"juan123","1234","juan","lopez","juancompany@gmail.com","juanlopez@gmail.com","jodar",true, c1.getTime(),"http//:imagen.com",c2.getTime(), Role.USER,2,2);
        Asignatura asignatura = new Asignatura(1,"lengua","castellano", LocalDate.of(2024,5,10),LocalDate.of(2025,5,10));
        List<Asignatura> asignaturaList = new ArrayList<>();
        asignaturaList.add(asignatura);
        Student student = new Student(1,10,"bueno","ciencia",persona,asignaturaList,new Profesor());
        StudentInputDto studentInputDto = modelMapper.map(student,StudentInputDto.class);
        StudentInputDto studentInputDto1 = studentDtoConverter.converToSimpleOutput(student);
        assertEquals(studentInputDto.toString(),studentInputDto1.toString());
    }

    @Test
    void converToFullOutput() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024,5,20);
        Calendar c2 = Calendar.getInstance();
        c2.set(2050,5,20);
        Persona persona = new Persona(1,"juan123","1234","juan","lopez","juancompany@gmail.com","juanlopez@gmail.com","jodar",true, c1.getTime(),"http//:imagen.com",c2.getTime(), Role.USER,2,2);
        Asignatura asignatura = new Asignatura(1,"lengua","castellano", LocalDate.of(2024,5,10),LocalDate.of(2025,5,10));
        List<Asignatura> asignaturaList = new ArrayList<>();
        asignaturaList.add(asignatura);
        Student student = new Student(1,10,"bueno","ciencia",persona,asignaturaList,new Profesor());
        StudentOutputDto studentOutputDto = modelMapper.map(student, StudentOutputDto.class);
        StudentOutputDto studentOutputDto1 = studentDtoConverter.converToFullOutput(student);
        assertEquals(studentOutputDto.toString(),studentOutputDto1.toString());
    }


}