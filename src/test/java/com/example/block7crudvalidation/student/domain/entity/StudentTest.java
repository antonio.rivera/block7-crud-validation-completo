package com.example.block7crudvalidation.student.domain.entity;

import com.example.block7crudvalidation.asignatura.domain.entity.Asignatura;
import com.example.block7crudvalidation.persona.domain.entity.Persona;
import com.example.block7crudvalidation.persona.domain.enums.Role;
import com.example.block7crudvalidation.profesor.domain.entity.Profesor;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class StudentTest {
    @Test
    void getSetId_profesor() {
        Profesor profesor = new Profesor();
        profesor.setId_profesor(1);
        assertEquals(1,profesor.getId_profesor());
    }

    @Test
    void getSetComents() {
        Profesor profesor = new Profesor();
        profesor.setComents("bueno");
        assertEquals("bueno",profesor.getComents());
    }

    @Test
    void getSetBranch() {
        Profesor profesor = new Profesor();
        profesor.setBranch("ciencia");
        assertEquals("ciencia",profesor.getBranch());
    }

    @Test
    void getSetPersona() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024,5,20);
        Calendar c2 = Calendar.getInstance();
        c2.set(2050,5,20);
        Persona persona = new Persona(1,"juan123","1234","juan","lopez","juancompany@gmail.com","juanlopez@gmail.com","jodar",true, c1.getTime(),"http//:imagen.com",c2.getTime(), Role.USER,2,2);
        Profesor profesor = new Profesor();
        profesor.setPersona(persona);
        assertEquals(profesor.getPersona(),persona);
    }

    @Test
    void getSetStudents() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024,5,20);
        Calendar c2 = Calendar.getInstance();
        c2.set(2050,5,20);
        Persona persona = new Persona(1,"juan123","1234","juan","lopez","juancompany@gmail.com","juanlopez@gmail.com","jodar",true, c1.getTime(),"http//:imagen.com",c2.getTime(),Role.USER,2,2);
        Asignatura asignatura = new Asignatura(1,"lengua","castellano", LocalDate.of(2024,5,10),LocalDate.of(2025,5,10));
        List<Asignatura> asignaturaList = new ArrayList<>();
        asignaturaList.add(asignatura);
        Student student = new Student(1,10,"bueno","ciencia",persona,asignaturaList,new Profesor());
        List<Student> studentList = new ArrayList<>();
        studentList.add(student);
        Profesor profesor = new Profesor();
        profesor.setStudents(studentList);
        assertEquals(profesor.getStudents(), studentList);
    }

}