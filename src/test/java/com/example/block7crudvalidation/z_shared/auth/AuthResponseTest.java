package com.example.block7crudvalidation.z_shared.auth;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class AuthResponseTest {

    @Test
    void getToken() {
        AuthResponse authResponse = new AuthResponse();
        authResponse.setToken("gisndgviuognsaoevno");
        assertEquals("gisndgviuognsaoevno",authResponse.getToken());
    }

    @Test
    void testToString() {
        AuthResponse authResponse = new AuthResponse();
        authResponse.setToken("gisndgviuognsaoevno");
        assertEquals("AuthResponse(token=gisndgviuognsaoevno)",authResponse.toString());
    }

    @Test
    void builder() {
        AuthResponse authResponse1 = new AuthResponse("gisndgviuognsaoevno");
        AuthResponse authResponse = AuthResponse.builder()
                .token("gisndgviuognsaoevno").build();
        assertEquals(authResponse.toString(),authResponse1.toString());
    }
}