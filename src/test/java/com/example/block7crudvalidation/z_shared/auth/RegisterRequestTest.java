package com.example.block7crudvalidation.z_shared.auth;

import com.example.block7crudvalidation.persona.domain.enums.Role;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Calendar;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class RegisterRequestTest {

    @Test
    void getSetUsuario() {
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setUsuario("juan123");
        assertEquals("juan123",registerRequest.getUsuario());
    }

    @Test
    void getSetPassword() {
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setPassword("1234");
        assertEquals("1234",registerRequest.getPassword());
    }

    @Test
    void getSetName() {
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setName("juan");
        assertEquals("juan",registerRequest.getName());
    }

    @Test
    void getSetSurname() {
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setSurname("lopez");
        assertEquals("lopez",registerRequest.getSurname());
    }

    @Test
    void getSetCompany_email() {
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setCompany_email("juancompany@gmail.com");
        assertEquals("juancompany@gmail.com",registerRequest.getCompany_email());
    }

    @Test
    void getSetRegisterPersonal_email() {
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setPersonal_email("juanjodar@gmail.com");
        assertEquals("juanjodar@gmail.com",registerRequest.getPersonal_email());
    }

    @Test
    void getSetCity() {
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setCity("jodar");
        assertEquals("jodar",registerRequest.getCity());
    }

    @Test
    void getSetActive() {
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setActive(true);
        assertEquals(true,registerRequest.getActive());
    }

    @Test
    void getSetCreated_date() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024,5,20);
        Calendar c2 = Calendar.getInstance();
        c2.set(2024,5,20);
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setCreated_date(c1.getTime());
        assertEquals(registerRequest.getCreated_date(),c2.getTime());
    }

    @Test
    void getSetImagen_url() {
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setImagen_url("http//:imagen.com");
        assertEquals("http//:imagen.com",registerRequest.getImagen_url());
    }

    @Test
    void getSetTermination_date() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024,5,20);
        Calendar c2 = Calendar.getInstance();
        c2.set(2024,5,20);
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setTermination_date(c1.getTime());
        assertEquals(registerRequest.getTermination_date().toString(),c2.getTime().toString());
    }

    @Test
    void getSetRole() {
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setRole(Role.USER);
        assertEquals(Role.USER,registerRequest.getRole());
    }

    @Test
    void testToString() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024,5,21);
        Calendar c2 = Calendar.getInstance();
        c2.set(2024,5,21);
        RegisterRequest registerRequest = new RegisterRequest("juan123","1234","juan","lopez","juancompany@gmail.com","juanlopez@gmail.com","jodar",true, c1.getTime(),"http//:imagen.com",c2.getTime(),Role.USER);
        assertEquals("RegisterRequest(usuario=juan123, password=1234, name=juan, surname=lopez, company_email=juancompany@gmail.com, personal_email=juanlopez@gmail.com, city=jodar, active=true, created_date="+c1.getTime()+", imagen_url=http//:imagen.com, termination_date="+c2.getTime()+", role=USER)",registerRequest.toString());
    }

    @Test
    void builder() {
        Calendar c1 = Calendar.getInstance();
        c1.set(2024,5,21);
        Calendar c2 = Calendar.getInstance();
        c2.set(2024,5,21);
        RegisterRequest registerRequestEsperada = new RegisterRequest("juan123","1234","juan","lopez","juancompany@gmail.com","juanlopez@gmail.com","jodar",true, c1.getTime(),"http//:imagen.com",c2.getTime(),Role.USER);

        RegisterRequest registerRequestSimulada = RegisterRequest.builder()
                .usuario("juan123")
                .password("1234")
                .name("juan")
                .surname("lopez")
                .company_email("juancompany@gmail.com")
                .personal_email("juanlopez@gmail.com")
                .city("jodar")
                .active(true)
                .created_date(c1.getTime())
                .imagen_url("http//:imagen.com")
                .role(Role.USER)
                .termination_date(c2.getTime())
                .build();

        assertEquals(registerRequestSimulada.toString(),registerRequestEsperada.toString());
    }
}