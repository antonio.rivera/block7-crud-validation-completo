package com.example.block7crudvalidation.z_shared.auth;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class LoginRequestTest {

    @Test
    void getSetUsuario() {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsuario("juan123");
        assertEquals("juan123",loginRequest.getUsuario());
    }

    @Test
    void getSetPassword() {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setPassword("1234");
        assertEquals("1234",loginRequest.getPassword());
    }

    @Test
    void testToString() {
        LoginRequest loginRequest = new LoginRequest("juan123","1234");
        assertEquals("LoginRequest(usuario=juan123, password=1234)",loginRequest.toString());
    }

    @Test
    void builder() {
        LoginRequest loginRequestEsperada = new LoginRequest("juan123","1234");

        LoginRequest loginRequestSimulada = LoginRequest.builder()
                .usuario("juan123")
                .password("1234")
                .build();
        assertEquals(loginRequestSimulada.toString(),loginRequestEsperada.toString());

    }
}