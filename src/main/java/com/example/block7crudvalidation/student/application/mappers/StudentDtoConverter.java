package com.example.block7crudvalidation.student.application.mappers;

import com.example.block7crudvalidation.persona.infrastructure.repository.PersonaRepository;
import com.example.block7crudvalidation.student.domain.entity.Student;
import com.example.block7crudvalidation.student.infrastructure.controller.dto.input.StudentInputDto;
import com.example.block7crudvalidation.student.infrastructure.controller.dto.output.StudentOutputDto;
import com.example.block7crudvalidation.student.infrastructure.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;


@Component
@RequiredArgsConstructor
//@Builder
public class StudentDtoConverter {
    private final ModelMapper modelMapper;
    private final PersonaRepository personaRepository;
    private final StudentRepository studentRepository;

    public Student converToInputStudent(StudentInputDto studentInputDto){
            return modelMapper.map(studentInputDto,Student.class);

    }

    public StudentInputDto converToSimpleOutput(Student student){
        return modelMapper.map(student,StudentInputDto.class);
    }

    public StudentOutputDto converToFullOutput(Student student){
        return modelMapper.map(student,StudentOutputDto.class);
    }
}
