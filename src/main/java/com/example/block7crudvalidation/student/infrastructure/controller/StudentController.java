package com.example.block7crudvalidation.student.infrastructure.controller;


import com.example.block7crudvalidation.persona.infrastructure.repository.PersonaRepository;
import com.example.block7crudvalidation.student.infrastructure.controller.dto.input.StudentInputDto;
import com.example.block7crudvalidation.student.infrastructure.controller.dto.output.StudentOutputDto;
import com.example.block7crudvalidation.student.application.mappers.StudentDtoConverter;
import com.example.block7crudvalidation.asignatura.domain.entity.Asignatura;
import com.example.block7crudvalidation.persona.domain.entity.Persona;
import com.example.block7crudvalidation.student.domain.entity.Student;
import com.example.block7crudvalidation.persona.application.PersonaService;
import com.example.block7crudvalidation.student.application.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping("/student")
public class StudentController {

    private final StudentService studentService;
    private final PersonaService personaService;
    private final StudentDtoConverter studentDtoConverter;
    private final PersonaRepository personaRepository;



    /**
     * Crear estudiante
     *
     * @param studentInputDto
     * @return Student
     */

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping("/crearStudent")
    public ResponseEntity<Student> crearStudent(@RequestBody StudentInputDto studentInputDto){
        // A la hora de crear un estudiante compruebo que no es profesor
        Persona persona = personaRepository.findById(studentInputDto.getId_persona()).orElse(null);
        Student student = studentDtoConverter.converToInputStudent(studentInputDto);
        if (persona.getProfesor_id()==null){
            //persona.setIsStudent(true);
            //persona.setStudent_id(studentInputDto.getId_student());
            //studentInputDto.setPersona(persona);
            //persona.setStudent(modelMapper.map(studentInputDto,Student.class));
            //return modelMapper.map(studentInputDto,Student.class);
            student.setPersona(persona);
            persona.setStudent_id(studentService.crearStudent(student).getId_student());
            personaRepository.save(persona);
            return ResponseEntity.ok(student);
        }

        return null;
    }



    /**
     * Mostrar todos los estudiantes
     *
     * @return List<Student>
     */
    @PreAuthorize("hasAuthority('USER')")
    @GetMapping("/mostrarStudents")
    public ResponseEntity<List<Student>> mostrarStudents(){
        return ResponseEntity.ok(studentService.mostrarStudents());
    }


    /**
     * Mostrar estudiante por id
     *
     * @param id_student
     * @param outputType
     * @return ?
     */
    @PreAuthorize("hasAuthority('USER')")
    @GetMapping("/{id_student}")
    public ResponseEntity<?> mostrarStudentPorId(@PathVariable Integer id_student, @RequestParam(value = "outputType",defaultValue = "simple") String outputType){
        if (outputType.equals("full")){
            StudentOutputDto studentOutputDto =
                    studentDtoConverter
                            .converToFullOutput(studentService.mostrarPorId(id_student));
            return ResponseEntity.ok(studentOutputDto);
        } else if (outputType.equals("simple")) {
            StudentInputDto studentInputDto =
                    studentDtoConverter
                            .converToSimpleOutput(studentService.mostrarPorId(id_student));
            return ResponseEntity.ok(studentInputDto);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();

    }

    /**
     * Modificar estudiante
     * @param id_student
     * @param student
     * @return Student
     */
    @PreAuthorize("hasAuthority('ADMIN')")
    @PutMapping("/modificarStudent/{id_student}")
    public ResponseEntity<Student> modificarStudent(@PathVariable Integer id_student, @RequestBody Student student){
        return ResponseEntity.ok(studentService.modificarStudent(id_student,student));
    }



    /**
     * Eliminar estudiante
     * @param id_student
     * @return Student
     */
    @PreAuthorize("hasAuthority('ADMIN')")
    @DeleteMapping("/eliminarStudent/{id_student}")
    public ResponseEntity<Student> eliminarStudent(@PathVariable Integer id_student){
        return ResponseEntity.ok(studentService.eliminarStudent(id_student));
    }


    /**
     * Mostrar todas las asignaturas que tiene un estudiante
     * @param id
     * @return List<Asignatura>
     */
    @PreAuthorize("hasAuthority('USER')")
    @GetMapping("/estudiante-asignatura/{id}")
    public ResponseEntity<List<Asignatura>> estudianteAsignatura(@PathVariable Integer id){
        return ResponseEntity.ok(studentService.estudianteAsignaturas(id));
    }



    /**
     * Agregar asignaturas a un estudiante
     *
     * @param id
     * @param listaAsignaturas
     * @return Student
     */
    @PreAuthorize("hasAuthority('ADMIN')")
    @PutMapping("/agregarAsignaturas/{id}")
    public ResponseEntity<Student> agregarAsignaturas(@PathVariable Integer id, @RequestBody List<Integer> listaAsignaturas ){
        return ResponseEntity.ok(studentService.agregarAsignaturas(listaAsignaturas,id));
    }

    /**
     * Eliminar las asignaturas de un estudiante
     * @param id
     * @param listaAsignaturas
     * @return Student
     */
    @PreAuthorize("hasAuthority('ADMIN')")
    @PutMapping("/eliminarAsignaturas/{id}")
    public ResponseEntity<Student> eliminarAsignaturas(@PathVariable Integer id, @RequestBody List<Integer> listaAsignaturas){
        return ResponseEntity.ok(studentService.eliminarAsignaturas(listaAsignaturas, id));
    }


}
