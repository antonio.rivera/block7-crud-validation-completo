package com.example.block7crudvalidation.student.infrastructure.controller.dto.input;

import com.example.block7crudvalidation.persona.domain.entity.Persona;
import lombok.*;

@Getter @Setter @ToString
@AllArgsConstructor @NoArgsConstructor
public class StudentInputDto {
    private Integer id_student;//  [pk, increment]
    private Integer num_hours_week;
    private String coments;
    private String branch;
    private Integer id_persona;
    private Persona persona;
}
