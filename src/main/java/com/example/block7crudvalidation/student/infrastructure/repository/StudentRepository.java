package com.example.block7crudvalidation.student.infrastructure.repository;

import com.example.block7crudvalidation.student.domain.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends JpaRepository<Student,Integer> {
    //Optional<Persona> findById(String id_student);
    // Optional<Persona> findByUsuario(String usuario);
}
