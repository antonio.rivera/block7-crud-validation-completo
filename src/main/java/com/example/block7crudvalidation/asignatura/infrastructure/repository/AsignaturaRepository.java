package com.example.block7crudvalidation.asignatura.infrastructure.repository;

import com.example.block7crudvalidation.asignatura.domain.entity.Asignatura;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AsignaturaRepository extends JpaRepository<Asignatura,Integer> {
    // Optional<Persona> findByUsuario(String usuario);
}
