package com.example.block7crudvalidation.asignatura.infrastructure.controller;

import com.example.block7crudvalidation.asignatura.domain.entity.Asignatura;
import com.example.block7crudvalidation.asignatura.application.AsignaturaService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/asignaturas")
@RequiredArgsConstructor
public class AsignaturaController {
    private final AsignaturaService asignaturaService;

    /**
     * Mostrar todas las asignaturas
     *
     * @return List<Asignatura>
     */
    @PreAuthorize("hasAuthority('USER')")
    @GetMapping
    public ResponseEntity<List<Asignatura>> mostrarAsignaturas(){
        return ResponseEntity.ok(asignaturaService.mostrarAsignaturas());
    }


    /**
     * Mostrar por id una asignatura
     *
     * @param id
     * @return Asignatura
     */
    @PreAuthorize("hasAuthority('USER')")
    @GetMapping("/{id}")
    public ResponseEntity<Asignatura> mostrarPorId(@PathVariable Integer id){
        return ResponseEntity.ok(asignaturaService.mostrarPorId(id));
    }

    /**
     * Crear una asignatura
     *
     * @param asignatura
     * @return Asignatura
     */
    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping("/crearAsignatura")
    public ResponseEntity<Asignatura> crearAsignatura(@RequestBody Asignatura asignatura){
        return ResponseEntity.ok(asignaturaService.crearAsignatura(asignatura));
    }

    /**
     * Modificar asignatura
     * @param id_asignatura
     * @param asignatura
     * @return Asignatura
     */

    @PreAuthorize("hasAuthority('ADMIN')")
    @PutMapping("/modificar/{id_asignatura}")
    public ResponseEntity<Asignatura> modificarAsignatura(@PathVariable Integer id_asignatura, @RequestBody Asignatura asignatura){
        return ResponseEntity.ok(asignaturaService.modificarAsignatura(id_asignatura,asignatura));
    }


    /**
     * Eliminar asignatura
     * @param id
     * @return Asignatura
     */

    @PreAuthorize("hasAuthority('ADMIN')")
    @DeleteMapping("/borrar/{id}")
    public ResponseEntity<Asignatura> eliminarAsignatura(@PathVariable Integer id){
        return ResponseEntity.ok(asignaturaService.eliminarAsignatura(id));
    }

}
