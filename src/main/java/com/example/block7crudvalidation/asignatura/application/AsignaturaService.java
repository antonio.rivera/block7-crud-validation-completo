package com.example.block7crudvalidation.asignatura.application;

import com.example.block7crudvalidation.asignatura.domain.entity.Asignatura;

import java.util.List;

public interface AsignaturaService {
    // Crear
    Asignatura crearAsignatura(Asignatura asignatura);

    // Mostrar todos
    List<Asignatura> mostrarAsignaturas();

    // Mostrar por id
    Asignatura mostrarPorId(Integer id_asignatura);

    // Modificar
    Asignatura modificarAsignatura(Integer id_asignatura, Asignatura asignatura);

    // Eliminar
    Asignatura eliminarAsignatura(Integer id_asignatura);
}
