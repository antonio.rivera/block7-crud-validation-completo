package com.example.block7crudvalidation.profesor.domain.entity;

import com.example.block7crudvalidation.student.domain.entity.Student;
import com.example.block7crudvalidation.persona.domain.entity.Persona;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Getter @Setter @ToString
@AllArgsConstructor @NoArgsConstructor
@Table(name = "profesor")
public class Profesor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_profesor;// [pk, increment]
    //private String id_persona;//  string [ref:- persona.id_persona] -- Relación OneToOne con la tabla persona.
    private String coments;//  string
    private String branch;//  string [not null] -- Materia principal que imparte. Por ejemplo: Front

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_persona")
    private Persona persona;

    @OneToMany(mappedBy = "profesor", cascade = CascadeType.ALL)
    private List<Student> students;
}
