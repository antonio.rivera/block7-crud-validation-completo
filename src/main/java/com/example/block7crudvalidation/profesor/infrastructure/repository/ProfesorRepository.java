package com.example.block7crudvalidation.profesor.infrastructure.repository;

import com.example.block7crudvalidation.profesor.domain.entity.Profesor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfesorRepository extends JpaRepository<Profesor,Integer> {
    // Optional<Persona> findByUsuario(String usuario);
}
