package com.example.block7crudvalidation.profesor.infrastructure.controller.dto.input;


import lombok.*;

@Getter @Setter @AllArgsConstructor @NoArgsConstructor @ToString
public class ProfesorInputDto {
    private Integer id_profesor;// [pk, increment]
    private String coments;//  string
    private String branch;
    private Integer id_persona;
}
