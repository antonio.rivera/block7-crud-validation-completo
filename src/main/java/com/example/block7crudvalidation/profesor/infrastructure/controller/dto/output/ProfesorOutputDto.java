package com.example.block7crudvalidation.profesor.infrastructure.controller.dto.output;

import com.example.block7crudvalidation.persona.domain.entity.Persona;
import lombok.*;


@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ProfesorOutputDto {
    private Integer id_profesor;// [pk, increment]
    private String coments;//  string
    private String branch;
    private Integer id_persona;
    private Persona persona;
}
