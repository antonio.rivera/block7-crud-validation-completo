package com.example.block7crudvalidation.profesor.application.mappers;


import com.example.block7crudvalidation.profesor.domain.entity.Profesor;
import com.example.block7crudvalidation.profesor.infrastructure.controller.dto.input.ProfesorInputDto;
import com.example.block7crudvalidation.profesor.infrastructure.controller.dto.output.ProfesorOutputDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProfesorDtoConverter {

    @Autowired
    private ModelMapper modelMapper;
    public Profesor convertInputToProfesor(ProfesorInputDto profesorInputDto){
        return modelMapper.map(profesorInputDto,Profesor.class);
    }
    public ProfesorInputDto converToSimpleOutput(Profesor profesor){
        return modelMapper.map(profesor,ProfesorInputDto.class);
    }

    public ProfesorOutputDto converToFullOutput(Profesor profesor){
        return modelMapper.map(profesor, ProfesorOutputDto.class);
    }
}
