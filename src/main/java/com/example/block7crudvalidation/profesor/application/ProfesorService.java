package com.example.block7crudvalidation.profesor.application;

import com.example.block7crudvalidation.profesor.domain.entity.Profesor;

import java.util.List;

public interface ProfesorService {
    // Crear
    Profesor crearProfesor(Profesor profesor);

    // Mostrar todos
    List<Profesor> mostrarProfesores();

    // Mostrar por id
    Profesor mostrarPorId(Integer id_profesor);

    // Modificar
    Profesor modificarProfesor(Integer id_profesor, Profesor profesor);

    // Eliminar
    Profesor eliminarProfesor(Integer id_profesor);
}
