package com.example.block7crudvalidation.persona.application;

import com.example.block7crudvalidation.persona.domain.entity.Persona;

import java.util.List;

public interface PersonaService {

    Persona findPersonaById(Integer id_persona);

    Persona findByCampoUsuario(String user);

    List<Persona> mostrarPersonas();

    Persona crearPersona(Persona persona) throws Exception;

    Persona modificarStudentPersona(Persona persona);

    Persona eliminarPersona(Integer id);

}
