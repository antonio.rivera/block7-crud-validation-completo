package com.example.block7crudvalidation.persona.application.mappers;

import com.example.block7crudvalidation.persona.infrastructure.controller.dto.input.PersonaDto;
import com.example.block7crudvalidation.persona.infrastructure.controller.dto.output.PersonaDtoFullOutput;
import com.example.block7crudvalidation.persona.infrastructure.controller.dto.output.PersonaDtoSimpleOutput;
import com.example.block7crudvalidation.persona.domain.entity.Persona;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class PersonaDtoConverter {
    private final ModelMapper modelMapper;

    public PersonaDto converToDto(Persona persona){
        return modelMapper.map(persona,PersonaDto.class);
    }

    public PersonaDtoSimpleOutput simpleOutput(Persona persona){
        return modelMapper.map(persona,PersonaDtoSimpleOutput.class);
    }

    public PersonaDtoFullOutput fullOutput(Persona persona){
        return modelMapper.map(persona,PersonaDtoFullOutput.class);
    }

}
