package com.example.block7crudvalidation.persona.infrastructure.controller;

import com.example.block7crudvalidation.persona.application.PersonaService;
import com.example.block7crudvalidation.persona.application.mappers.PersonaDtoConverter;
import com.example.block7crudvalidation.persona.domain.entity.Persona;
import com.example.block7crudvalidation.persona.infrastructure.controller.dto.input.PersonaDto;
import com.example.block7crudvalidation.persona.infrastructure.controller.dto.output.PersonaDtoFullOutput;
import com.example.block7crudvalidation.persona.infrastructure.controller.dto.output.PersonaDtoSimpleOutput;
import com.example.block7crudvalidation.persona.infrastructure.repository.PersonaRepository;
import com.example.block7crudvalidation.profesor.application.ProfesorService;
import com.example.block7crudvalidation.profesor.domain.entity.Profesor;
import com.example.block7crudvalidation.student.application.StudentService;
import com.example.block7crudvalidation.student.domain.entity.Student;
import com.example.block7crudvalidation.z_shared.specification.SearchPersonaSpecification;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "http://127.0.0.1:5500")
public class PersonaController {

    private final PersonaService personaService;
    private final PersonaDtoConverter personaDtoConverter;
    private final StudentService studentService;
    private final ProfesorService profesorService;
    private final PersonaRepository personaRepository;

    /**
     * Crea una persona
     *
     * @param persona
     * @return personaDto
     */
    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping("/persona/crearPersona")
    public ResponseEntity<PersonaDto> crearPersona(@RequestBody Persona persona) throws Exception {
        //personaService.crearPersona(persona);
        PersonaDto personaDto = personaDtoConverter
                .converToDto(personaService.crearPersona(persona));
        return ResponseEntity.ok(personaDto);
    }




    /**
     * Busca persona por id_persona
     *
     * @param id
     * @param outputType
     * @return personaDtoFullOutput o personaDtoSimpleOutput
     */

    @PreAuthorize("hasAuthority('USER')")
    @GetMapping("/persona/porId/{id}")
    //@PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> findPersonaById(@PathVariable Integer id, @RequestParam(value = "outputType", defaultValue = "simple") String outputType) {
        if (outputType.equals("full")) {
            // Tengo que localizar el profesor o estudiante primero
            Persona persona = personaService.findPersonaById(id);
            PersonaDtoFullOutput personaDtoFullOutput = new PersonaDtoFullOutput();
            //PersonaDtoSimpleOutput personaDtoSimpleOutput = new PersonaDtoSimpleOutput();
            if (persona.getStudent_id() != null) {
                Student student = studentService.mostrarPorId(persona.getStudent_id());
                personaDtoFullOutput = personaDtoConverter.fullOutput(persona);
                personaDtoFullOutput.setStudent(student);
                return ResponseEntity.ok(personaDtoFullOutput);
            } else if (persona.getProfesor_id() != null) {
                Profesor profesor = profesorService.mostrarPorId(persona.getProfesor_id());
                personaDtoFullOutput = personaDtoConverter.fullOutput(persona);
                personaDtoFullOutput.setProfesor(profesor);
                return ResponseEntity.ok(personaDtoFullOutput);
            }
        }
        return ResponseEntity.ok(personaDtoConverter.simpleOutput(personaService.findPersonaById(id)));
    }

    /**
     * Busca persona por usuario
     *
     * @param usuario
     * @return personaDto
     */

    @PreAuthorize("hasAuthority('USER')")
    @GetMapping("/persona/porUsuario/{usuario}")
    public ResponseEntity<?> findByCampoUsuario(@PathVariable String usuario, @RequestParam(value = "outputType", defaultValue = "simple") String outputType){
        Persona persona = personaService.findByCampoUsuario(usuario);
        if (outputType.equals("full")) {
             //Tengo que localizar el profesor o estudiante primero

            PersonaDtoFullOutput personaDtoFullOutput = new PersonaDtoFullOutput();
            //PersonaDtoSimpleOutput personaDtoSimpleOutput = new PersonaDtoSimpleOutput();
            if (persona.getStudent_id()!=null){
                System.out.println("1");
                Student student = studentService.mostrarPorId(persona.getStudent_id());
                personaDtoFullOutput = personaDtoConverter.fullOutput(persona);
                personaDtoFullOutput.setStudent(student);
                return ResponseEntity.ok(personaDtoFullOutput);
            } else if (persona.getProfesor_id()!=null){
                System.out.println("2");
                Profesor profesor = profesorService.mostrarPorId(persona.getProfesor_id());
                personaDtoFullOutput = personaDtoConverter.fullOutput(persona);
                personaDtoFullOutput.setProfesor(profesor);
                return ResponseEntity.ok(personaDtoFullOutput);
            }
            //System.out.println("2");
        }
            //return personaService.findByCampoUsuario(user);
            //return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        //PersonaDtoFullOutput personaDtoFullOutput = personaDtoConverter.fullOutput(personaService.findByCampoUsuario(usuario));
        //return ResponseEntity.ok(personaDtoFullOutput);
        // return ResponseEntity.ok(personaService.findByCampoUsuario(usuario));
        return ResponseEntity.ok(personaDtoConverter.simpleOutput(persona));
    }


    /**
     * Muestra todas las personas
     * @param outputType
     * @return List<PersonaDtoFullOutput> o List<PersonaDtoSimpleOutput>
     */
    @GetMapping("/personas")
    @PreAuthorize("hasAuthority('USER')") // @PreAuthorize("hasAnyAuthority(['USER','ADMIN'])") @PreAuthorize("hasAuthority('USER')||hasAuthority('ADMIN')")
     public ResponseEntity<List<?>> mostrarPersonas(@RequestParam(value = "outputType", defaultValue = "simple") String outputType){
        List<Persona> personas = personaService.mostrarPersonas();
        if(outputType.equals("full")){
            List<PersonaDtoFullOutput> personaDtoFullOutputList = new ArrayList<>();
            for(Persona p:personas){
                if (p.getProfesor_id()!=null){
                    PersonaDtoFullOutput personaDtoFullOutput = personaDtoConverter.fullOutput(p);
                    personaDtoFullOutput.setProfesor(profesorService.mostrarPorId(p.getProfesor_id()));
                    personaDtoFullOutputList.add(personaDtoFullOutput);
                }
                if (p.getStudent_id()!=null){
                    PersonaDtoFullOutput personaDtoFullOutput = personaDtoConverter.fullOutput(p);
                    personaDtoFullOutput.setStudent(studentService.mostrarPorId(p.getStudent_id()));
                    personaDtoFullOutputList.add(personaDtoFullOutput);
                }
            }
            return ResponseEntity.ok(personaDtoFullOutputList);
        } else if (outputType.equals("simple")){
            List<PersonaDtoSimpleOutput> personaDtoSimpleOutputs = new ArrayList<>();
            for(Persona p:personas){
                personaDtoSimpleOutputs.add(personaDtoConverter.simpleOutput(p));
            }
            return ResponseEntity.ok(personaDtoSimpleOutputs);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
     }

    /**
     * Elimina una persona
     * @param id
     * @return
     */
    @PreAuthorize("hasAuthority('ADMIN')")
     @DeleteMapping("/borrarPersona/{id}")
    public ResponseEntity<Persona> borrarPersona(@PathVariable Integer id){
        return ResponseEntity.ok(personaService.eliminarPersona(id));
     }


    /**
     * Obtiene un profesor por id
     * @param id
     * @return
     */
    @PreAuthorize("hasAuthority('USER')")
    @GetMapping("/persona/profesor/{id}")
    public ResponseEntity<Profesor> getProfesorById(@PathVariable Integer id){
        List<Persona> personaList = personaService.mostrarPersonas();
        Profesor profesor = new Profesor();
        for (Persona p:personaList){
            if (p.getProfesor_id()==id){
                profesor = profesorService.mostrarPorId(p.getProfesor_id());
            }
        }
        //Profesor profesor = profesorService.mostrarPorId(personaService.findPersonaById(id).getProfesor_id());
        return ResponseEntity.ok(profesor);
    }

    /**
     * Filtra las personas segun los parametros
     * @param usuario
     * @param name
     * @param surname
     * @param arriba
     * @param abajo
     * @param fecha_limite
     * @param numPagina
     * @param tamañoPaginas
     * @return
     */
    @PreAuthorize("hasAuthority('USER')")
   @GetMapping("/criteria")
    public ResponseEntity<Page<Persona>> criteria(@RequestParam(required = false,defaultValue = "false") boolean usuario,
                                                  @RequestParam(required = false,defaultValue = "false") boolean name,
                                                  @RequestParam(required = false,defaultValue = "false") boolean surname,
                                                  @RequestParam(required = false,defaultValue = "false") boolean arriba,
                                                  @RequestParam(required = false,defaultValue = "false") boolean abajo,
                                                  @RequestParam(required = false) Date fecha_limite,
                                                  @RequestParam(required = true) int numPagina,
                                                  @RequestParam(required = false,defaultValue = "10") int tamañoPaginas){
       SearchPersonaSpecification searchPersonaSpecification = new SearchPersonaSpecification(usuario,name,surname,fecha_limite,arriba,abajo);
       final Pageable pageable = PageRequest.of(numPagina,tamañoPaginas);
       return ResponseEntity.ok(personaRepository.findAll(searchPersonaSpecification,pageable));

    }



}
