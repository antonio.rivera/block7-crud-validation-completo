package com.example.block7crudvalidation.persona.infrastructure.repository;

import com.example.block7crudvalidation.persona.domain.entity.Persona;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PersonaRepository extends JpaRepository<Persona,Integer>, JpaSpecificationExecutor<Persona> {
    Optional<Persona> findByUsuario(String usuario);
}
