package com.example.block7crudvalidation.persona.infrastructure.controller.dto.input;

import lombok.*;

import java.util.Date;

@Getter @Setter @ToString
@AllArgsConstructor @NoArgsConstructor
public class PersonaDto {
    //POner id
    private Integer idPersona;
    private String usuario;//  [not null max-length: 10 min-length: 6]
    private String password;// [not null]
    private String name;// [not null]
    private String surname;
    private String companyEmail;// [not null ]
    private String personalEmail;// [not null]
    private String city;// [not null]
    private Boolean active;// [not null]
    private Date createdDate;// [not null]
    private String imagenUrl;
    private Date terminationDate;
    private Integer studentId;


    private Integer profesorId;

}
