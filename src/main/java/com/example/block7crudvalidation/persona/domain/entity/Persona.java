package com.example.block7crudvalidation.persona.domain.entity;

import com.example.block7crudvalidation.persona.domain.enums.Role;
import jakarta.persistence.*;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Date;
import java.util.List;

@Entity
@Getter @Setter @ToString
@AllArgsConstructor @NoArgsConstructor
@Table(name = "persona",uniqueConstraints = {@UniqueConstraint(columnNames = {"usuario"})})
@Builder
public class Persona implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_persona;


    private String usuario;//  [not null max-length: 10 min-length: 6]
    private String password;// [not null]
    private String name;// [not null]
    private String surname;
    private String company_email;// [not null ]
    private String personal_email;// [not null]
    private String city;// [not null]
    private Boolean active;// [not null]
    private Date created_date;// [not null]
    private String imagen_url;
    private Date termination_date;

    @Enumerated(EnumType.STRING)
    private Role role;
    //private boolean admin;


    private Integer student_id;


    private Integer profesor_id;


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {

        return List.of(new SimpleGrantedAuthority(role.name()));
    }

    /*@Override
    public Collection<? extends GrantedAuthority> getAuthorities() {

        return List.of(new SimpleGrantedAuthority("admin"));
    }*/

    @Override
    public String getUsername() {
        return this.usuario;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
