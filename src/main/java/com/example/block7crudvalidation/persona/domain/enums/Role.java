package com.example.block7crudvalidation.persona.domain.enums;

public enum Role {
    ADMIN,
    USER
}
