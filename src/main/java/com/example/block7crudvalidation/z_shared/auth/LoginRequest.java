package com.example.block7crudvalidation.z_shared.auth;

import lombok.*;

@ToString
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoginRequest {
    private String usuario;
    private String password;
}
