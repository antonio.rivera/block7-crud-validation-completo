package com.example.block7crudvalidation.z_shared.auth;


import com.example.block7crudvalidation.persona.domain.enums.Role;
import lombok.*;

import java.util.Date;

@ToString
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegisterRequest {
    private String usuario;//  [not null max-length: 10 min-length: 6]
    private String password;// [not null]
    private String name;// [not null]
    private String surname;
    private String company_email;// [not null ]
    private String personal_email;// [not null]
    private String city;// [not null]
    private Boolean active;// [not null]
    private Date created_date;// [not null]
    private String imagen_url;
    private Date termination_date;
    private Role role;
}
