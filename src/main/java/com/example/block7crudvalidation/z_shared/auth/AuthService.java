package com.example.block7crudvalidation.z_shared.auth;


import com.example.block7crudvalidation.z_shared.jwt.JwtService;
import com.example.block7crudvalidation.persona.domain.entity.Persona;
import com.example.block7crudvalidation.persona.infrastructure.repository.PersonaRepository;
import com.example.block7crudvalidation.persona.domain.enums.Role;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthService {
    private final PersonaRepository personaRepository;
    private final JwtService jwtService;
    private final PasswordEncoder passwordEncoder;
    private final AuthenticationManager authenticationManager;
    public AuthResponse login(LoginRequest loginRequest) {
         authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsuario(),loginRequest.getPassword()));
        UserDetails userDetails = personaRepository.findByUsuario(loginRequest.getUsuario()).orElseThrow();
        String token = jwtService.getToken(userDetails);

        return AuthResponse.builder().token(token).build();
    }

    public AuthResponse register(RegisterRequest registerRequest) {
        Persona persona = Persona.builder()
                .usuario(registerRequest.getUsuario())
                .password(passwordEncoder.encode(registerRequest.getPassword()))
                .city(registerRequest.getCity())
                .name(registerRequest.getName())
                .active(registerRequest.getActive())
                .created_date(registerRequest.getCreated_date())
                .company_email(registerRequest.getCompany_email())
                .personal_email(registerRequest.getPersonal_email())
                .surname(registerRequest.getSurname())
                .imagen_url(registerRequest.getImagen_url())
                .termination_date(registerRequest.getTermination_date())
                .role(Role.USER)
                .build();
        personaRepository.save(persona);
        return AuthResponse
                .builder()
                .token(jwtService.getToken(persona))
                .build();
    }
}
