package com.example.block7crudvalidation.z_shared.specification;

import com.example.block7crudvalidation.persona.domain.entity.Persona;
import jakarta.persistence.criteria.*;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
public class SearchPersonaSpecification implements Specification<Persona> {
    private boolean usuario;
    private boolean name;
    private boolean surname;
   // @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date fecha_limite;
    private boolean arriba;
    private boolean abajo;

    @Override
    public Predicate toPredicate(Root<Persona> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        //query.orderBy(criteriaBuilder.asc(root.get("usuario")));
        List<Predicate> predicateList = new ArrayList<>();
        if (usuario){
            query.orderBy(criteriaBuilder.asc(root.get("usuario")));
        }
        if (name){
            query.orderBy(criteriaBuilder.asc(root.get("name")));
        }
        if (surname){
            query.orderBy(criteriaBuilder.asc(root.get("surname")));
        }
        if (arriba){
            Predicate fechaPorEncima = criteriaBuilder.greaterThan(root.get("created_date"),fecha_limite);
            predicateList.add(fechaPorEncima);
        }
        if (abajo){
            Predicate fechaPorDebajo = criteriaBuilder.lessThan(root.get("created_date"),fecha_limite);
            predicateList.add(fechaPorDebajo);
        }
        if (abajo&&arriba){
            query.orderBy(criteriaBuilder.asc(root.get("created_date")));
        }



        return criteriaBuilder.and(predicateList.toArray(new Predicate[predicateList.size()]));
    }
}
