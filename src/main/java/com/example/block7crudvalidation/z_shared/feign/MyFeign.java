package com.example.block7crudvalidation.z_shared.feign;

import com.example.block7crudvalidation.profesor.domain.entity.Profesor;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(url = "http://localhost:8081", name = "myFeign")
public interface MyFeign {
    @GetMapping("/profesor/{id}")
    Profesor getProfesorById(@PathVariable("id") Integer id);
}
