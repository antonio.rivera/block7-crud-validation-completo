package com.example.block7crudvalidation.z_shared.feign;


import com.example.block7crudvalidation.profesor.domain.entity.Profesor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyController {
    @Autowired
    private final MyFeign myFeign;

    @Autowired
    public MyController(MyFeign myFeign) {
        this.myFeign = myFeign;
    }

    @GetMapping("/profesor/{id}")
    public Profesor getProfesorById(@PathVariable Integer id){
        return myFeign.getProfesorById(id);
    }
}
